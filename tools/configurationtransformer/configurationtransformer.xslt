<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" />

    <xsl:param name="Configuration" />
    
    <xsl:template match="*">        
        <xsl:if test="not(@configuration) or contains(@configuration, $Configuration)">
            <xsl:copy>            
                <xsl:for-each select="@*[name() != 'configuration']">                    
                    <xsl:copy />
                </xsl:for-each>
              
                <xsl:apply-templates />
            </xsl:copy>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>