This is version 2.2 of the nxslt.exe and NxsltTask for MSBuild and NAnt.

Changes:

o improved error reporting
o options files (@file option)
o XHTML output mode
o XSLT 2.0 character maps
o nxslt and NxsltTask are now debuggable

See http://www.xmllab.net/nxslt for more info.