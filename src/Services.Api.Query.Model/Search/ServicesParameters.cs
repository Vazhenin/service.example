﻿using System;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.Model.Search
{
    /// <summary>
    /// Параметры поиска услуг
    /// </summary>
    public class TripPlanParameters : PagingParameters
    {
        /// <summary>
        /// Минимальное значение
        /// </summary>
        public DateTime? MinimumEndOn { get; set; }

        /// <summary>
        /// Максимальное значение
        /// </summary>
        public DateTime? MaximumEndOn { get; set; }

        /// <summary>
        /// Идентификатор путешественника
        /// </summary>
        public int? TravellerProfileId { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public UInt64? Version { get; set; }
    }
}
