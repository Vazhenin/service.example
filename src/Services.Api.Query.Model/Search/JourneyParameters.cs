﻿using System;
using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Model.Search
{
    public class JourneyParameters
    {
        /// <summary>
        /// Размер страницы.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Номер страницы.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Номер поездки.
        /// </summary>
        public int? JourneyNumber { get; set; }

        /// <summary>
        /// Номер командировки.
        /// </summary>
        public int? TripNumber { get; set; }

        /// <summary>
        /// Номер поездки или командировки.
        /// </summary>
        public int? JourneyOrTripNumber { get; set; }

        /// <summary>
        /// Идентификаторы профилей для поиска.
        /// </summary>
        public int[] ProfileIds { get; set; }

        /// <summary>
        /// Идентификаторы компаний для поиска.
        /// </summary>
        public int[] CompanyIds { get; set; }

        /// <summary>
        /// Город отправления.
        /// </summary>
        public int? TravelFromCityId { get; set; }

        /// <summary>
        /// Город назначения.
        /// </summary>
        public int? TravelToCityId { get; set; }

        /// <summary>
        /// Дата начала поиска (без времени).
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Дата окончания поиска (без времени).
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Ограничение по дате начала (поездка должна начинаться до указанной даты)
        /// </summary>
        public DateTime? StartsBeforeDate { get; set; }

        /// <summary>
        /// Ограничение по дате окончания (поездка должна оканчиваться после указанной даты)
        /// </summary>
        public DateTime? EndsAfterDate { get; set; }
        /// <summary>
        /// Номер заказа, используется для поиска конкретного заказа.
        /// </summary>
        public int? OrderNumber { get; set; }

        /// <summary>
        /// Состояние услуги.
        /// </summary>
        public ServiceState[] ServiceState { get; set; }

        /// <summary>
        /// Типы услуг.
        /// </summary>
        public ServiceType[] ServiceType { get; set; }

        /// <summary>
        /// Идентификаторы путешественников.
        /// </summary>
        public int[] TravellerProfileIds { get; set; }

        /// <summary>
        /// Идентификаторы путешественников.
        /// </summary>
        [Obsolete("Выпилить через пару обновлений. Использовать массив.")]
        public int? TravellerProfileId { get; set; }

        /// <summary>
        /// Фильтр по статусу.
        /// </summary>
        public TripStatus[] StatusFilter { get; set; }
    }
}
