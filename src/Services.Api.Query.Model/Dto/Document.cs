﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    public class Document
    {
        public string TicketToken { get; set; }

        public Resource[] Attachments { get; set; }
    }
}
