﻿using System;
using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Model.Dto
{
    public class BusinessTripInfo : BusinessTripProfileId
    {
        public int CompanyId { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedByProfileId { get; set; }
        public int Number { get; set; }
        public int JourneyNumber { get; set; }
        public AuthorizationAssertion AuthorizationAssertion { get; set; }
        public AuthorizationStatus AuthorizationStatus { get; set; }
        public int TravelFromCityId { get; set; }
        public int JourneyToCityId { get; set; }
        public ServiceCard[] Services { get; set; }
        public decimal? Budget { get; set; }

        /// <summary>
        /// Количество активных услуг.
        /// </summary>
        public int ActiveServiceCount { get; set; }

        /// <summary>
        /// Количество услуг.
        /// </summary>
        public int ServiceCount { get; set; }
    }
}
