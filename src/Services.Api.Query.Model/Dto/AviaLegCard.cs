﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Model.Dto
{
    public class AviaLegCard : AviaLegBase
    {
        public AviaSegmentCard[] Segments { get; set; }

        /// <summary>
        /// Количество пересадок
        /// </summary>
        public int Transfers { get; set; }
    }
}
