﻿namespace Services.Api.Query.Model.Dto
{
    public class Principal
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public int? ProfileId { get; set; }
    }
}