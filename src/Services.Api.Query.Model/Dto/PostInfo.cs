﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Базовая информация по сообщению
    /// </summary>
    public class PostInfo
    {
        /// <summary>
        /// Идентфикатор сообщения
        /// Нужен для получений новых сообщений относительно этого идентификатора
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Автор сообщения (необходимо разрезолвить в ProfileId или EmployeeId)
        /// </summary>
        public int? PostedByProfileId { get; set; }

        /// <summary>
        /// Время добавления сообщения
        /// </summary>
        public DateTime? PostedAt { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Message { get; set; }
    }
}
