﻿namespace Services.Api.Query.Model.Dto
{
    public class AviaServiceLite : ServiceCard
    {
        public AviaLegLite[] Legs { get; set; }
    }
}
