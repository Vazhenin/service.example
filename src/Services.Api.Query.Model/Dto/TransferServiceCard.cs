﻿using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка трансферной услуги - краткая информация
    /// </summary>
    public class TransferServiceCard : ServiceCard
    {
        public CarClassification? CarClassification { get; set; }


        public int? DepartureFromCityId { get; set; }

        public int? ArrivalToCityId { get; set; }

        public int? DepartureFromType { get; set; }

        public string DepartureFromLocation { get; set; }

        public int? DepartureFromAirportId { get; set; }
        public int? DepartureFromRailwayStationId { get; set; }


        public string FlightNumber { get; set; }
        public string PickupCardText { get; set; }

        public int? ArrivalToType { get; set; }
        public string ArrivalToLocation { get; set; }
        public int? ArrivalToRailwayStationId { get; set; }
        public int? ArrivalToAirportId { get; set; }



        public string DepartureFromAddressRu { get; set; }
        public string DepartureFromAddressEn { get; set; }
        public string ArrivalToAddressRu { get; set; }
        public string ArrivalToAddressEn { get; set; }


    }
}
