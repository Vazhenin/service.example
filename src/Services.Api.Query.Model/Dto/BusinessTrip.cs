﻿using Services.Api.Query.Model.Enum;
using System;

namespace Services.Api.Query.Model.Dto
{
    public class BusinessTrip : BusinessTripInfo
    {
        public AuthorizationAssertion CompleteTripAuthorizationAssertion { get; set; }
        public AuthorizationStatus CompleteTripAuthorizationStatus { get; set; }

        public PaymentType? RequisitesPaymentMethod { get; set; }
        public int? RequisitesBankCardId { get; set; }

        public CustomPropertyValue[] CustomPropertyValues { get; set; }
        public BusinessTripExpenses[] BusinessTripExpenses { get; set; }

        public bool HasDocuments { get; set; }
    }
}
