﻿using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Model.Dto
{
    public class AviaSegmentCard
    {
        public CabinClass CabinClass { get; set; }

        public int? BaggageQuantity { get; set; }

        public int? BaggageWeightMeasurement { get; set; }

        public int? BaggageType { get; set; }

        public string MarketingAirlineCode { get; set; }
    }
}
