﻿namespace Services.Api.Query.Model.Dto
{
    public class HotelServiceLite : HotelServiceCard
    {
        public string RoomNameEn { get; set; }

        public string ConfirmationToken { get; set; }

        public decimal? EarlyCheckinCost { get; set; }
        public decimal? LateCheckoutCost { get; set; }

        public bool? IsPayOnSite { get; set; }
    }
}
