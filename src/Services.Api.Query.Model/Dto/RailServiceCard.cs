﻿namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка ЖД-услуги - краткая информация
    /// </summary>
    public class RailServiceCard : ServiceCard
    {
        public string TrainCode { get; set; }
        /// <summary>
        /// Имя поезда.
        /// </summary>
        public string TrainName { get; set; }
        public string PublicTrainCode { get; set; }

        public string TrainCategory { get; set; }

        public string CarriageType { get; set; }
        public string CarriageNumber { get; set; }


        public int? DepartureFromStationId { get; set; }
        public int? ArrivalToStationId { get; set; }

        public bool? IsElectronicRegistrationAllowed { get; set; }

        public long? Duration { get; set; }

        public int? DirectionGroup { get; set; }
        public string CarriageClass { get; set; }
        public string CarriageOwner { get; set; }
    }
}
