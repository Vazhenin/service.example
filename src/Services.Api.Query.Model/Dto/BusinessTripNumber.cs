﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Полный номер командировки
    /// </summary>
    public class BusinessTripNumber
    {
        /// <summary>
        /// Номер поездки
        /// </summary>
        public int JourneyNumber { get; set; }

        /// <summary>
        /// Номер командировки
        /// </summary>
        public int TripNumber { get; set; }

    }
}
