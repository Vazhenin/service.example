﻿using Services.Api.Query.Model.Enum;
using System.Collections.Generic;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка авиа-услуги - краткая информация
    /// </summary>
    public class AviaServiceCard : ServiceCard
    {
        public AviaLegCard[] Legs { get; set; }
    }
}
