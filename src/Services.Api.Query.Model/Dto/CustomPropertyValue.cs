﻿namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Значение доп данного.
    /// </summary>
    public class CustomPropertyValue
    {
        /// <summary>
        /// Русское имя.
        /// </summary>
        public string NameRu { get; set; }

        /// <summary>
        /// Латинское имя.
        /// </summary>
        public string NameEn { get; set; }

        /// <summary>
        /// Латинское значение.
        /// </summary>
        public string ValueEn { get; set; }

        /// <summary>
        /// Русское значение.
        /// </summary>
        public string ValueRu { get; set; }

    }
}
