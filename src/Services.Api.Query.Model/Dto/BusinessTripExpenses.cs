﻿namespace Services.Api.Query.Model.Dto
{
    public class BusinessTripExpenses
    {
      
        public decimal Cost { get; set; }

        public string ExpensesSnapshotNameRu { get; set; }

        public string ExpensesSnapshotNameEn { get; set; }

    }
}
