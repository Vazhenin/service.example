﻿using Services.Api.Query.Model.Enum;
using System;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка услуги аэроэкспресс - краткая информация
    /// </summary>
    public class AeroExpressServiceCard : ServiceCard
    {
        public AeroExpressTariffClass TariffClass { get; set; }
        public TimeSpan? DepartureAt { get; set; }
        public TimeSpan? ArrivalAt { get; set; }

        public int DepartureStation { get; set; }
        public int ArrivalStation { get; set; }
    }
}
