﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    public abstract class AviaLegBase
    {
        public int? DepartureFromAirportId { get; set; }
        public int? ArrivalToAirportId { get; set; }

        public DateTime? DepartureAt { get; set; }

        public DateTime? ArrivalAt { get; set; }

        public long? Duration { get; set; }
    }
}
