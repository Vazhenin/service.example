﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    public class Resource
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public int ContentLength { get; set; }

        public string Fingerprint { get; set; }
    }
}
