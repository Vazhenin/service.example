﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Информация по 
    /// </summary>
    public class BusinessTripProfileId : BusinessTripId
    {

        /// <summary>
        /// Профиль пассажира, на которого оформлена командировка
        /// </summary>
        public int ProfileId { get; set; }
    }
}
