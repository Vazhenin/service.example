﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Сообщение в чате
    /// </summary>
    public class Post : PostInfo
    {
       

        //public bool IsPrivate { get; set; }

        //public ServicingStatus ServicingStatus { get; set; }

        //public int Type { get; set; }

        //public int OrderId { get; set; }

        /// <summary>
        /// Прикрепленные файлы к сообщению
        /// </summary>
        public Resource[] Attachments { get; set; }

        /// <summary>
        /// Прочитано ли сообщение
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// Сообщение принадлежит самому отправляющему
        /// </summary>
        public bool IsPersonMessage { get; set; }


        /// <summary>
        /// Версия сообщения. Преимущественно используется для получения более новых сообщений
        /// </summary>
        public ulong Version { get; set; }
    }
}
