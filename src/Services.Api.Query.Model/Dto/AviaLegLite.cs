﻿namespace Services.Api.Query.Model.Dto
{
    public class AviaLegLite : AviaLegBase
    {
        public int Id { get; set; }

        public AviaSegmentLite[] Segments { get; set; }

    }
}
