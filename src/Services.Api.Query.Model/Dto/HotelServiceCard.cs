﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка отельной услуги - краткая информация
    /// </summary>
    public class HotelServiceCard : ServiceCard
    {
        public int? HotelId { get; set; }
        public int? CityId { get; set; }

        public string RoomNameRu { get; set; }

        public int? SaleType { get; set; }

        public DateTime? ActualCheckinAt { get; set; }

        public DateTime? ActualCheckoutAt { get; set; }

        public HotelMealCard[] Meals { get; set; }
    }
}
