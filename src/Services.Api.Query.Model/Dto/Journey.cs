﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Model.Dto
{
    public class Journey
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedByProfileId { get; set; }
        public int Number { get; set; }
        public int JourneyToCityId { get; set; }
        public DateTime StartsOn { get; set; }
        public DateTime EndsOn { get; set; }
        public int CompanyId { get; set; }
        public DateTime? DeletedAt { get; set; }
        public int? DeletedByPrincipalId { get; set; }
        public BusinessTrip[] BusinessTrips { get; set; }
        
    }
}
