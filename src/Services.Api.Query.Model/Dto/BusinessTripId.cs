﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Описание командировки
    /// </summary>
    public class BusinessTripId
    {
        /// <summary>
        /// Id командировки
        /// </summary>
        public int Id { get; set; }
    }
}
