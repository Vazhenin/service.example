﻿namespace Services.Api.Query.Model.Dto.Search
{
    /// <summary>
    /// Пареметры для получения сообщений
    /// </summary>
    public class PostParameters
    {
        /// <summary>
        /// Идентификатор последнего полученного сообщения. 
        /// Если указан, то возвращаются все сообщения с Id меньше указанного
        /// </summary>
        public int? LastMessageId { get; set; }

        /// <summary>
        /// Версия сообщения. Если указана, то возвращаются только сообщения с версией выше.
        /// </summary>
        public long? VersionToken { get; set; }


        /// <summary>
        /// Количество сообщений, которое необходимо вернуть.
        /// Максимум 10
        /// </summary>
        public int? Last { get; set; } = 10;

        /// <summary>
        /// Профайл, для которого будем проверять прочитанность
        /// </summary>
        public int? ReadByProfileId { get; set; }
    }
}
