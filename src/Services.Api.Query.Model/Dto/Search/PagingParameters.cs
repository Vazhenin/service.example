﻿namespace Services.Api.Query.Model.Dto.Search
{
    /// <summary>
    /// Данные для страничности поисковой выборки.
    /// </summary>
    public abstract class PagingParameters
    {
        /// <summary>
        /// Размер страницы.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Номер страницы.
        /// </summary>
        public int PageNumber { get; set; }
    }
}