﻿using System;
using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Model.Dto.Search
{
    /// <summary>
    /// Данные для поисковой выборки поездок.
    /// </summary>
    public class JourneyParameters : PagingParameters
    {
        /// <summary>
        /// Номер поездки.
        /// </summary>
        public int? JourneyNumber { get; set; }

        /// <summary>
        /// Номер командировки.
        /// </summary>
        public int? TripNumber { get; set; }

        /// <summary>
        /// Номер поездки или командировки.
        /// </summary>
        public int? JourneyOrTripNumber { get; set; }

        /// <summary>
        /// Идентификаторы профилей для поиска.
        /// </summary>
        public string ProfileIds { get; set; }

        /// <summary>
        /// Идентификаторы компаний для поиска.
        /// </summary>
        public string CompanyIds { get; set; }

        /// <summary>
        /// Город отправления.
        /// </summary>
        public int? TravelFromCityId { get; set; }

        /// <summary>
        /// Город назначения.
        /// </summary>
        public int? TravelToCityId { get; set; }

        /// <summary>
        /// Дата начала поиска (без времени).
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Дата окончания поиска (без времени).
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Ограничение по дате начала (поездка должна начинаться до указанной даты)
        /// </summary>
        public DateTime? StartsBeforeDate { get; set; }

        /// <summary>
        /// Ограничение по дате окончания (поездка должна оканчиваться после указанной даты)
        /// </summary>
        public DateTime? EndsAfterDate { get; set; }
        /// <summary>
        /// Номер заказа, используется для поиска конкретного заказа.
        /// </summary>
        public int? OrderNumber { get; set; }

        /// <summary>
        /// Состояние услуги.
        /// </summary>
        public ServiceState[] ServiceState { get; set; }

        /// <summary>
        /// Типы услуг.
        /// </summary>
        public ServiceType[] ServiceType { get; set; }

        /// <summary>
        /// Идентификатор путешественника.
        /// </summary>
        [Obsolete("Выпилить через пару обновлений. Использовать массив.")]
        public int? TravellerProfileId { get; set; }

        /// <summary>
        /// Идентификаторы путешественников.
        /// </summary>
        public string TravellerProfileIds { get; set; }

        /// <summary>
        /// Фильтр по статусу.
        /// </summary>
        public TripStatus[] StatusFilter { get; set; }
    }
}