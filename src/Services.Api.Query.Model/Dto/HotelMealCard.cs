﻿namespace Services.Api.Query.Model.Dto
{
    /// <summary>
    /// Карточка питания в отеле - краткая информация
    /// </summary>
    public class HotelMealCard
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsIncludedInPrice { get; set; }

    }
}
