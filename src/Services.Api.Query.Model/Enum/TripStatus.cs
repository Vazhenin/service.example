﻿namespace Services.Api.Query.Model.Enum
{
    /// <summary>
    /// Статус командировки.
    /// </summary>
    public enum TripStatus
    {
        /// <summary>
        /// Черновик.
        /// </summary>
        Draft,

        /// <summary>
        /// В процессе.
        /// </summary>
        Active,

        /// <summary>
        /// Выполнено.
        /// </summary>
        Complete,

        /// <summary>
        /// Закрыто.
        /// </summary>
        Closed
    }
}
