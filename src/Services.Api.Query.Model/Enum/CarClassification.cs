﻿namespace Services.Api.Query.Model.Enum
{
    public enum CarClassification
    {
        Any = 0,
        
        Standart = 1,
        
        Business = 2,
        
        Minivan = 3,
        
        Minibus = 4,
        
        Econom = 5,
        
        Representative = 6,
        
        Bus = 7
    }
}
