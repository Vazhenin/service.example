﻿namespace Services.Api.Query.Model.Enum
{
    /// <summary>
    /// Состояние услуги.
    /// </summary>
    public enum ServiceState
    {
        /// <summary>
        /// Черновик.
        /// </summary>
        Unknown,

        /// <summary>
        /// Забронировано.
        /// </summary>
        Reservation,

        /// <summary>
        /// Отменено.
        /// </summary>
        Refunding,

        /// <summary>
        /// Обслуживается.
        /// </summary>
        Servicing,

        /// <summary>
        /// Оформлено.
        /// </summary>
        Execution
    }
}