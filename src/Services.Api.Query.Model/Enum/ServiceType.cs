﻿namespace Services.Api.Query.Model.Enum
{
    public enum ServiceType
    {
        Unknown,
        Avia,
        Rail,
        Hotel,
        AeroExpress,
        Insurance,
        Transfer,
        Visa,
        Vip,
        CarRental,
        RailEurope
    }
}
