﻿namespace Services.Api.Query.Model.Enum
{
    public enum ServiceStatus
    {
        
        Draft,
        
        Archived,
        
        Active
    }
}
