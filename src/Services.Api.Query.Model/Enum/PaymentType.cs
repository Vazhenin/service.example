﻿namespace Services.Api.Query.Model.Enum
{
    public enum PaymentType
    {
        Unknown = 0,
        
        Cash = 1,
        
        Invoice = 2,
        
        Bta = 3,
        
        CreditCard = 4,
        
        CreditCardNoSof = 5,
        
        Debit = 7
    }
}