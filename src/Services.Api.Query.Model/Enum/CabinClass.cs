﻿namespace Services.Api.Query.Model.Enum
{
    public enum CabinClass
    {
        NotSet = 0,

        Economy = 1,

        Business = 2,

        First = 3,
    }
}
