﻿namespace Services.Api.Query.Model.Enum
{
    public enum RefundStatus
    {
        
        Undefined = 0,
        
        Pending = 1,
        
        Succeeded = 2,
        
        Failed = 3
    }
}
