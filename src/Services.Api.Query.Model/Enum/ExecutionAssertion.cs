﻿namespace Services.Api.Query.Model.Enum
{
    public enum ExecutionAssertion
    {
        NotSupported,
        
        Available,
        
        Unavailable
    }
}
