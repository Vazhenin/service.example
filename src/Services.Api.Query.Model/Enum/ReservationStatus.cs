﻿namespace Services.Api.Query.Model.Enum
{
    public enum ReservationStatus
    {
        
        Pending,
        
        InProgress,
        
        Succeeded,
        
        Failed,
        
        Cancelled,
        
        Expired,
        
        Rejected
    }
}
