﻿namespace Services.Api.Query.Model.Enum
{
    public enum ExecutionStatus
    {
        Pending,
        
        InProgress,
        
        Succeeded,
        
        Failed,
        
        Cancelled,
        
        Rejected
    }
}
