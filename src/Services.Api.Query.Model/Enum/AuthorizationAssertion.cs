﻿namespace Services.Api.Query.Model.Enum
{
    public enum AuthorizationAssertion
    {
        NotSupported,
        Required,
        NotRequired
    }
}