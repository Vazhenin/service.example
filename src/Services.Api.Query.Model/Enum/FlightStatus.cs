﻿namespace Services.Api.Query.Model.Enum
{
    public enum FlightStatus
    {
        Unknown,

        Scheduled,

        DepartureDelayed,

        ArrivalDelayed,

        Cancelled
    }
}
