﻿namespace Services.Api.Query.Model.Enum
{
    public enum AuthorizationStatus
    {
        
        NotSupported,

        /// <summary>
        /// Ожидание начала процесса авторизации.
        /// </summary>
        Pending,

        /// <summary>
        /// Авторизация выполняется.
        /// </summary>
        InProgress,

        /// <summary>
        /// Успешная авторизация.
        /// </summary>
        Authorized,

        /// <summary>
        /// Отказ в авторизации.
        /// </summary>
        NotAuthorized,

        /// <summary>
        /// Отзыва запроса авторизации заказчиком
        /// </summary>
        Recalled
    }
}