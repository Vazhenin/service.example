﻿namespace Services.Api.Query.Model.Enum
{
    public enum AeroExpressTariffClass
    {
        Any = 0,
        
        Economy = 1,
        
        Business = 2
    }
}
