﻿namespace Services.Api.Query.Model.Enum
{
    public enum ServicingRequest
    {
        
        Undefined = 0,
        
        Reservation = 1,
        
        Execution = 2,
        
        Cancellation = 3,
        
        Alteration = 4,
        
        PriceInquiry = 5,
        
        DeferredExecution = 6
    }
}
