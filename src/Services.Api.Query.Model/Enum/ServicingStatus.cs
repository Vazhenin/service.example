﻿namespace Services.Api.Query.Model.Enum
{
    public enum ServicingStatus
    {
        Undefined,
        Completed,
        Pending,
        InProgress,
        ResponseWaiting
    }
}