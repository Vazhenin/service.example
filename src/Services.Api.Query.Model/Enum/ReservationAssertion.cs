﻿namespace Services.Api.Query.Model.Enum
{
    public enum ReservationAssertion
    {
        
        NotSupported,

        
        Available,

        
        Unavailable
    }
}
