﻿using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Model.Parameters
{
    /// <summary>
    /// Параметры для получения номера командировки
    /// </summary>
    public class BusinessTripIdParameter : IQueryParameters<BusinessTripNumber>
    {
        public const string BusinessTripNumberByIdActionName = "BusinessTripNumberById";

        /// <summary>
        /// Id командировки
        /// </summary>
        public int TripId { get; set; }


        string IQueryParameters<BusinessTripNumber>.ActionName => BusinessTripNumberByIdActionName;

    }
}
