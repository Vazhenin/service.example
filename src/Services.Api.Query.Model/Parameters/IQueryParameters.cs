﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
[assembly: InternalsVisibleTo("Services.Api.Query.WebApi")]
namespace Services.Api.Query.Model.Parameters
{
    
    /// <summary>
    /// Это параметры, по которым надо получить информацию
    /// </summary>
    public interface IQueryParameters
    {
    }

    /// <summary>
    /// Это запрос, который содержит параметры для получения необходимой информации
    /// </summary>
    /// <typeparam name="TResult">Это тип, который будет содержать выходную информацию</typeparam>
    public interface IQueryParameters<TResult> : IQueryParameters
    {
        /// <summary>
        /// Название action для вызова в контроллере Parameters
        /// </summary>
        string ActionName { get; }

    }

}
