﻿using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Model.Parameters
{
    /// <summary>
    /// Параметры ProfileId и ServiceId для получения информации
    /// </summary>
    public class ServiceIdProfileIdParameter : IQueryParameters<int?>
    {
        public const string TravellerIdByServiceIdProfileIdActionName = "TravellerIdByServiceIdProfileId";

        /// <summary>
        /// Id услуги
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// Id пассажира в услуге
        /// </summary>
        public int ProfileId { get; set; }


        string IQueryParameters<int?>.ActionName => TravellerIdByServiceIdProfileIdActionName;

    }
}
