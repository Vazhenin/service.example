﻿using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Model.Parameters
{
    /// <summary>
    /// Параметры для получения информации по Id поездки
    /// </summary>
    public class JourneyIdParameter : IQueryParameters<int?>
    {
        public const string JourneyNumberByIdActionName = "JourneyNumberById";

        /// <summary>
        /// Id командировки
        /// </summary>
        public int JourneyId { get; set; }


        string IQueryParameters<int?>.ActionName => JourneyNumberByIdActionName;

    }
}
