﻿using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Model.Parameters
{
    /// <summary>
    /// Параметры для получения информации по номеру командировки
    /// </summary>
    public class BusinessTripNumberParameter : BusinessTripNumber,
        IQueryParameters<BusinessTripProfileId>,
        IQueryParameters<int?>
    {
        public const string BusinessTripProfileByBusinessTripNumberActionName = "BusinessTripProfileByBusinessTripNumber";
        public const string BusinessTripIdByNumberActionName = "BusinessTripIdByNumber";

        string IQueryParameters<BusinessTripProfileId>.ActionName => BusinessTripProfileByBusinessTripNumberActionName;

        string IQueryParameters<int?>.ActionName => BusinessTripIdByNumberActionName;
    }
}
