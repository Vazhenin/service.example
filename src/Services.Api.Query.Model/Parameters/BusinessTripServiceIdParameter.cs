﻿using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Model.Parameters
{
    /// <summary>
    /// Параметры для получения номера командировки
    /// </summary>
    public class BusinessTripServiceIdParameter : IQueryParameters<BusinessTripNumber>
    {
        public const string BusinessTripNumberByServiceIdActionName = "BusinessTripNumberByServiceId";

        /// <summary>
        /// Id командировки
        /// </summary>
        public int ServiceId { get; set; }


        string IQueryParameters<BusinessTripNumber>.ActionName => BusinessTripNumberByServiceIdActionName;

    }
}
