﻿using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Mapping;
using Autofac;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Api.Query.WebApi.Modules
{
    public class QueryParametersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var mappingAssemblies = new[] { typeof(IQueryCommand).Assembly };


            #region Resolver

            builder.RegisterAssemblyTypes(mappingAssemblies)
                .AsClosedTypesOf(typeof(IQueryCommand<,>))
                .AsSelf()
                .InstancePerLifetimeScope();

            #endregion
        }
    }
}