﻿using Services.Api.Query.Services.Mapping;
using Autofac;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Api.Query.WebApi.Modules
{
    public class AutomapperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var mappingAssemblies = new[] { typeof(MappingProfile).Assembly };


            builder.Register(c => new MapperConfiguration(x =>
            {
                x.AddProfiles(mappingAssemblies);
            })).As<IConfigurationProvider>().SingleInstance();

            builder.Register(ctx => ctx.Resolve<IComponentContext>()
                                    .Resolve<IConfigurationProvider>()
                                    .CreateMapper(ctx.Resolve<IComponentContext>().Resolve)).As<IMapper>();


            #region Resolver

            builder.RegisterAssemblyTypes(mappingAssemblies)
                .AsClosedTypesOf(typeof(IValueResolver<,,>))
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(mappingAssemblies)
                .AsClosedTypesOf(typeof(IMemberValueResolver<,,,>))
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(mappingAssemblies)
                .AsClosedTypesOf(typeof(ITypeConverter<,>))
                .AsSelf()
                .InstancePerLifetimeScope();

            #endregion
        }
    }
}