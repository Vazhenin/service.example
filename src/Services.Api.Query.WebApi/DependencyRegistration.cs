﻿using Services.Api.Query.Services;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.WebApi.Modules;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace Services.Api.Query.WebApi
{
    public static class DependencyRegistration
    {
        public static void RegisterDependencies(this IAppBuilder appBuilder, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<AutomapperModule>();
            builder.RegisterModule<QueryParametersModule>();


            var cacheConnectionString = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["cacheConnectionString"]);
            var redisDatabaseIndex =
                int.Parse(Environment.ExpandEnvironmentVariables(
                    ConfigurationManager.AppSettings["redisDatabaseIndex"]));

            //builder.Register(
            //    c => Mapper.Configuration.CreateMapper(c.Resolve))
            //    .As<IMapper>()
            //    .InstancePerLifetimeScope();

            builder.Register(c => new RedisCacheManager(cacheConnectionString)).As<IRedisCacheManager>().InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var cacheManager = c.Resolve<IRedisCacheManager>();
                return new PrincipalCache(cacheManager, redisDatabaseIndex);
            }).As<IPrincipalCache>().InstancePerLifetimeScope();


            var connectionString = Environment.ExpandEnvironmentVariables(ConfigurationManager.ConnectionStrings["cabinet"].ConnectionString);
            var options = new DbContextOptionsBuilder();

            options.UseSqlServer(connectionString);
            builder.Register(s => new CabinetDbContext(options.Options)).AsSelf().InstancePerRequest();

            builder.RegisterType<JourneyService>().As<IJourneyService>().InstancePerLifetimeScope();
            builder.RegisterType<TripService>().As<ITripService>().InstancePerLifetimeScope();
            builder.RegisterType<ParametersService>().As<IParametersService>().InstancePerLifetimeScope();
            builder.RegisterType<PrincipalResolver>().As<IPrincipalResolver>().InstancePerLifetimeScope();
            builder.RegisterType<TripPlanService>().As<ITripPlanService>().InstancePerLifetimeScope();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            appBuilder.UseAutofacWebApi(config);

            appBuilder.UseAutofacMiddleware(container);
        }
    }
}