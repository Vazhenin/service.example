﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Services.Api.Query.WebApi.Extensions
{
    static class ObjectExtensions
    {
        public static T ToObject<T>(this IEnumerable<KeyValuePair<string, string>> source)
            where T : class, new()
        {
            var someObject = new T();
            var someObjectType = someObject.GetType();

            foreach (var item in source)
            {
                var property = someObjectType.GetProperty(item.Key, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                property.SetValue(someObject, Convert.ChangeType(item.Value, property.PropertyType), null);
            }

            return someObject;
        }
    }
}