﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Services.Api.Query.WebApi.Extensions
{
    public class ActionTracer : ApiControllerActionSelector
    {
        /// <summary>
        /// Tracer для сохранения имени controller и action во внутренние переменные
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <returns></returns>
        public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            var m = base.SelectAction(controllerContext);

            var controllerName = m.ControllerDescriptor.ControllerName;
            var actionName = m.ActionName;

            controllerContext.Request.GetOwinEnvironment()["controller"] = controllerName;
            controllerContext.Request.GetOwinEnvironment()["action"] = actionName;

            MappedDiagnosticsLogicalContext.Set("action", actionName);
            MappedDiagnosticsLogicalContext.Set("controller", controllerName);

            return m;
        }       
    }


}