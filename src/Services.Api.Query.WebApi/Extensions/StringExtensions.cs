﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Services.Api.Query.WebApi.Extensions
{
    public static class StringExtensions
    {
        private static readonly Regex Dasher = new Regex(@"(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])", RegexOptions.Compiled | RegexOptions.Singleline);

        public static string ToUnderscoredLowercase(this string value)
        {
            var segments = Dasher.Split(value);
            return string.Join("_", segments.Select(s => s.ToLowerInvariant()));
        }
    }
}