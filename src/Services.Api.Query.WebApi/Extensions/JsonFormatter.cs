﻿using Services.Api.Query.WebApi.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Services.Api.Query.WebApi.Extensions
{
    public static class JsonFormatter
    {
        public static void ResponseAsJson(this HttpConfiguration config)
        {
            config.Formatters.Clear();
            BrowserJsonFormatter browserJsonFormatter = new BrowserJsonFormatter();
            browserJsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
            config.Formatters.Add(browserJsonFormatter);
        }
        private class BrowserJsonFormatter : JsonMediaTypeFormatter
        {
            public BrowserJsonFormatter()
            {
                //this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
                this.SerializerSettings.ContractResolver = new UnderscoreLowerCamelCaseContractResolver();
                this.SerializerSettings.Formatting = Formatting.Indented;
                this.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                this.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
            }

            public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
            {
                base.SetDefaultContentHeaders(type, headers, mediaType);
                headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
        }
    }

}