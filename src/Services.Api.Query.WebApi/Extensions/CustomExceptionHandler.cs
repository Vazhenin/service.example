﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace Services.Api.Query.WebApi.Extensions
{
    public class CustomExceptionLogger : ExceptionLogger
    {
        Logger _logger = LogManager.GetCurrentClassLogger();

        public override void Log(ExceptionLoggerContext context)
        {
            base.Log(context);

            _logger.Error(context.Exception);
        }
    }
}