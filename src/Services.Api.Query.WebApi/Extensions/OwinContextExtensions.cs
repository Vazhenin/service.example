﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Api.Query.WebApi.Extensions
{
    public static class OwinContextExtensions
    {
        public const string ActionTagName = "action";
        public const string ControllerTagName = "controller";
        const string ClientAssemblyVersionHeaderName = "x-client-version";

        public static string GetClientVersion(this IOwinContext owinContext)
        {
            return owinContext.Request.Headers[ClientAssemblyVersionHeaderName];
        }

        public static string GetActionName(this IOwinContext owinContext)
        {
            if (owinContext.Environment.TryGetValue(ActionTagName, out object actionName))
                return actionName.ToString().ToUnderscoredLowercase();
            return null;
        }

        public static string GetControllerName(this IOwinContext owinContext)
        {
            if (owinContext.Environment.TryGetValue(ControllerTagName, out object controllerName))
                return controllerName.ToString().ToUnderscoredLowercase();
            return null;
        }

        public static string GetUserAgent(this IOwinContext owinContext)
        {
            return owinContext.Request.Headers["user-agent"].ToString().ToLower();
        }
    }
}