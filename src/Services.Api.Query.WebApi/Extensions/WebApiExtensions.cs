﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Services.Api.Query.WebApi.Extensions
{
    public static class WebApiExtensions
    {
        public static void RegisterRoutes(this IAppBuilder appBuilder, HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ParametersContriller",
                routeTemplate: "parameters/{action}",
                defaults: new { controller = "parameters" }
            );
        }
    }
}