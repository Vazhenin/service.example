﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Services.Api.Query.WebApi.Tools
{
    public class UnderscoreLowerCamelCaseContractResolver : DefaultContractResolver
    {
        private static readonly Regex Dasher = new Regex(@"(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])", RegexOptions.Compiled | RegexOptions.Singleline);

        protected override string ResolvePropertyName(string propertyName)
        {
            return ToUnderscoredLowercase(propertyName);
        }

        private static string ToUnderscoredLowercase(string value)
        {
            var segments = Dasher.Split(value);
            return string.Join("_", segments.Select(s => s.ToLowerInvariant()));
        }
    }
}