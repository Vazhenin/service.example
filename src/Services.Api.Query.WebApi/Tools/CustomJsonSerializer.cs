﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Api.Query.WebApi.Tools
{
    public class CustomJsonSerializer : JsonSerializer
    {
        public CustomJsonSerializer()
        {
            ContractResolver = new UnderscoreLowerCamelCaseContractResolver();
            Formatting = Formatting.Indented;
        }
    }
}