﻿using Aeroclub.Services.Api.Query.WebApi.Extensions;
using Microsoft.Owin;
using NLog;
using Statsify.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Aeroclub.Services.Api.Query.WebApi.Middlewares
{
    /// <summary>
    /// Statsify оболочка
    /// </summary>
    public class StatsMiddleware : OwinMiddleware
    {
        private const string controllerTagName = "controller";
        private const string actionTagName = "action";
        private const string statusCodeTagName = "statuscode";


        public StatsMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            var stopwatch = Stopwatch.StartNew();

            await Next.Invoke(context).ConfigureAwait(false);

            var elapsed = stopwatch.Elapsed;

            var actionName = context.GetActionName();
            var controllerName = context.GetControllerName();

            if (actionName != null && controllerName != null)
            {
                var tags = new Dictionary<string, string>();
                tags["action"] = actionName;
                tags["controller"] = controllerName;
                tags["status"] = context.Response.StatusCode.ToString();
                tags["client_version"] = context.GetClientVersion();
                tags["user_agent"] = context.GetUserAgent();

                Stats.Time("requests", elapsed.TotalMilliseconds, tags: tags);

            }

        }
    }
}