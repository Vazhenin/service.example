﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using NLog;
using System.IO;
using Services.Api.Query.WebApi.Extensions;
using System.Diagnostics;

namespace Services.Api.Query.WebApi.Middlewares
{
    public class LoggerMiddleware : OwinMiddleware
    {
        const string CorrelationTokenName = "CorrelationToken";
        const string ParentCorrelationTokenName = "ParentCorrelationToken";

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public LoggerMiddleware(OwinMiddleware next) : base(next)
        {

        }

        public override async Task Invoke(IOwinContext context)
        {
            var correlationToken = Guid.NewGuid().ToString("N");

            MappedDiagnosticsLogicalContext.Set(CorrelationTokenName, correlationToken);
            MappedDiagnosticsLogicalContext.Set("user_agent", context.GetUserAgent());
            MappedDiagnosticsLogicalContext.Set("client_version", context.GetClientVersion());
            MappedDiagnosticsLogicalContext.Set("method", context.Request.Method);
            MappedDiagnosticsLogicalContext.Set("url", context.Request.Uri.ToString());


            var parentCorrelationToken = context.Request.Headers[CorrelationTokenName] ?? Guid.NewGuid().ToString("N");
            MappedDiagnosticsContext.Set(ParentCorrelationTokenName, parentCorrelationToken);

            var stopwatch = Stopwatch.StartNew();

            await Next.Invoke(context).ConfigureAwait(false);

            var elapsed = stopwatch.Elapsed;

            MappedDiagnosticsLogicalContext.Set("elapsedTime", elapsed.ToString("G"));

            MappedDiagnosticsLogicalContext.Set("status_code", context.Response.StatusCode.ToString());

            _logger.Trace(context.Request.Uri.ToString());

            context.Response.Headers["X-ElapsedTime"] = elapsed.ToString();
            context.Response.Headers[CorrelationTokenName] = parentCorrelationToken;
        }
    }
}