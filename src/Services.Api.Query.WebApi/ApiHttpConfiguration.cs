﻿using Services.Api.Query.WebApi.Extensions;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;

namespace Services.Api.Query.WebApi
{
    public class ApiHttpConfiguration : HttpConfiguration
    {
        public ApiHttpConfiguration()
        {
            this.ResponseAsJson();
            this.Services.Replace(typeof(IHttpActionSelector), new ActionTracer());
            this.Services.Replace(typeof(IExceptionLogger), new CustomExceptionLogger());

        }
    }
}