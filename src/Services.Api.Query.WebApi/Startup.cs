﻿using Services.Api.Query.WebApi.Extensions;
using Services.Api.Query.WebApi.Middlewares;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Services.Api.Query.WebApi.Startup))]

namespace Services.Api.Query.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new ApiHttpConfiguration();

            //appBuilder.Use<StatsMiddleware>();
            appBuilder.Use<LoggerMiddleware>();

            appBuilder.RegisterDependencies(config);
            appBuilder.RegisterRoutes(config);
            appBuilder.UseWebApi(config);
        }
    }
}
