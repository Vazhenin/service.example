﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web.Http;

namespace Services.Api.Query.WebApi.Controllers
{
    public class InfoController : ApiController
    {

        [Route("")]
        [HttpGet]
        public string GetInfo()
        {
            var version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            return $"Services.Api.Query v.{version} running on {Environment.MachineName}";
        }

        [Route("test_error")]
        [HttpGet]
        public string GetError()
        {
            throw new InvalidOperationException("Test exception");
        }

    }
}
