﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.WebApi.Controllers
{
    [RoutePrefix("journeys")]
    public class JourneyController : ApiController
    {
        private readonly IJourneyService _journeyService;

        public JourneyController(IJourneyService journeyService)
        {
            _journeyService = journeyService;
        }

        [Route("search")]
        [HttpGet]
        public IEnumerable<Journey> SearchJourneysPaged([FromUri] JourneyParameters dtoParameters)
        {
            return _journeyService.SearchJourneysPaged(dtoParameters);
        }

        [Route("{journeyNumber:int}/trips/{businessTripNumber:int}/unreadby/{profileId:int}")]
        [HttpGet]
        public bool UnreadPostsByNumbersAndProfile(int journeyNumber, int businessTripNumber, int profileId)
        {
            return _journeyService.GetPostUnReadStatusByNumbersAndProfile(journeyNumber, businessTripNumber, profileId);
        }
    }
}
