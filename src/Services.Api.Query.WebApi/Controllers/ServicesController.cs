﻿using System.Collections.Generic;
using System.Web.Http;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Search;
using Services.Api.Query.Services.Interfaces;

namespace Services.Api.Query.WebApi.Controllers
{
    [RoutePrefix("services")]
    public class ServicesController : ApiController
    {
        private readonly ITripPlanService _servicesService;

        public ServicesController(ITripPlanService servicesService)
        {
            _servicesService = servicesService;
        }

        [Route("tripPlan")]
        [HttpGet]
        public IEnumerable<ServiceCard> TripPlanPaged([FromUri] TripPlanParameters dtoParameters)
        {
            if (dtoParameters == null)
            {
                dtoParameters = new TripPlanParameters
                {
                    PageNumber = 0,
                    PageSize = 10
                };
            }

            return _servicesService.GetTripPlan(dtoParameters);
        }
    }
}