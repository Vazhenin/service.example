﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Commands;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.WebApi.Extensions;
using Autofac;
using System;
using System.Net.Http;
using System.Web.Http;

namespace Services.Api.Query.WebApi.Controllers
{
    public class ParametersController : ApiController
    {
        private readonly ILifetimeScope _scope;
        private readonly IParametersService _parametersService;

        public ParametersController(ILifetimeScope scope, IParametersService parametersService)
        {
            _parametersService = parametersService;
            _scope = scope;
        }


        [Route("parameters/trips/{tripid:int}/numbers")]
        [HttpGet]
        [Obsolete("Испольовать метод с параметрами")]
        public BusinessTripNumber GetTripNumbersByTripId(int tripid)
        {
            return _parametersService.GetTripNumbers(tripid);
        }

        [Route("parameters/journeys/{journeyid:int}/number")]
        [HttpGet]
        [Obsolete("Испольовать метод с параметрами")]
        public int GetJourneyNumberByJourneyId(int journeyid)
        {
            return _parametersService.GetJourneyNumber(journeyid);
        }

        [HttpGet]
        [ActionName(BusinessTripIdParameter.BusinessTripNumberByIdActionName)]
        public BusinessTripNumber GetBusinessTripNumberById()
        {
            return ExecuteCommand<GetTripNumberCommand, BusinessTripIdParameter, BusinessTripNumber>();
        }

        [HttpGet]
        [ActionName(BusinessTripServiceIdParameter.BusinessTripNumberByServiceIdActionName)]
        public BusinessTripNumber GetBusinessTripNumberByServiceId()
        {
            return ExecuteCommand<GetTripNumberCommand, BusinessTripServiceIdParameter, BusinessTripNumber>();
        }


        [HttpGet]
        [ActionName(JourneyIdParameter.JourneyNumberByIdActionName)]
        public int? GetJourneyNumberById()
        {
            return ExecuteCommand<GetJourneyNumberByIdCommand, JourneyIdParameter, int?>();
        }

        [HttpGet]
        [ActionName(BusinessTripNumberParameter.BusinessTripProfileByBusinessTripNumberActionName)]
        public BusinessTripProfileId GetTripProfileByTripNumber()
        {
            return ExecuteCommand<GetTripProfileIdByTripNumberCommand, BusinessTripNumberParameter, BusinessTripProfileId>();
        }

        [HttpGet]
        [ActionName(BusinessTripNumberParameter.BusinessTripIdByNumberActionName)]
        public int? GetBusinessTripIdByNumber()
        {
            return ExecuteCommand<GetTripIdByNumberCommand, BusinessTripNumberParameter, int?>();
        }

        [HttpGet]
        [ActionName(ServiceIdProfileIdParameter.TravellerIdByServiceIdProfileIdActionName)]
        public int? GetTravellerIdByProfileId()
        {
            return ExecuteCommand<GetTravellerIdByServiceIdProfileIdCommand, ServiceIdProfileIdParameter, int?>();
        }


        private TResult ExecuteCommand<TCommand, TQueryParameter, TResult>()
            where TQueryParameter : class, IQueryParameters<TResult>, new()
            where TCommand : IQueryCommand<TQueryParameter, TResult>
        {
            var parameters = Request.GetQueryNameValuePairs().ToObject<TQueryParameter>();
            var command = _scope.Resolve<TCommand>();
            var result = command.Execute(parameters);
            return result;
        }

    }
}
