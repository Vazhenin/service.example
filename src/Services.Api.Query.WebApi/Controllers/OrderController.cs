﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.WebApi.Controllers
{
    [RoutePrefix("orders")]
    public class OrderController : ApiController
    {
        private readonly IJourneyService _journeyService;

        public OrderController(IJourneyService journeyService)
        {
            _journeyService = journeyService;
        }

        [Route("{orderNumber}/journey")]
        [HttpGet]
        public Journey GetJourneyByOrderNumber(int orderNumber)
        {
            return _journeyService.GetJourneyByOrderNumber(orderNumber);
        }
    }
}