﻿using Aeroclub.Services.Api.Query.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Aeroclub.Services.Api.Query.Model;
using System.Web;

namespace Aeroclub.Services.Api.Query.WebApi.Controllers
{
    public class TestController : ApiController
    {

        [Route("")]
        [HttpGet]
        public string GetInfo()
        {
            return $"Aeroclub.Services.Api.Query v.{System.Reflection.Assembly.GetExecutingAssembly().GetName().Version} running on {Environment.MachineName}";
        }

    }
}
