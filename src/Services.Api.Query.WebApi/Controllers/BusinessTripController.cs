﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services.Api.Query.Model;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Dto.Search;
using Services.Api.Query.Services.Interfaces;

namespace Services.Api.Query.WebApi.Controllers
{
    public class BusinessTripController : ApiController
    {
        private readonly ITripService _businessTripService;

        public BusinessTripController(ITripService businessTripService)
        {
            _businessTripService = businessTripService;
        }

        [Route("trips/{tripid:int}")]
        [HttpGet]
        public string GetTripById(int tripid)
        {
            throw new NotImplementedException();
        }


        [Route("journeys/{journeyNumber:int}/trips/{tripNumber:int}")]
        [HttpGet]
        public BusinessTrip GeTripByNumber(int journeyNumber, int tripNumber)
        {
            var businessTrip = _businessTripService.GetTripByNumber(journeyNumber, tripNumber);
            
            return businessTrip;
        }

        [Route("trips/{tripId:int}/posts/{postId:int}")]
        [HttpGet]
        public PostInfo GetTripPost(int tripId, int postId)
        {
            return _businessTripService.GetTripPost(tripId, postId);
        }

        [Route("journeys/{journeyNumber:int}/trips/{tripNumber:int}/posts")]
        [HttpGet]
        public IEnumerable<Post> GetTripPosts(int journeyNumber, int tripNumber, [FromUri]PostParameters postParameters)
        {
            return _businessTripService.GetTripPosts(journeyNumber, tripNumber, postParameters);
        }

        [Route("journeys/{journeyNumber:int}/trips/{tripNumber:int}/documents")]
        [HttpGet]
        public IEnumerable<Document> GetTravellerDocumentsForTrip(int journeyNumber, int tripNumber)
        {
            return _businessTripService.GetTravellerDocumentsForTrip(journeyNumber, tripNumber);

        }
        
        [Route("trips/{tripid:int}/unreadby/{profileId:int}")]
        [HttpGet()]
        public bool UnreadPostsByIdAndProfile(int tripid, int profileId)
        {
            return _businessTripService.GetPostUnReadStatusByBusinessTripeIdsAndProfile(tripid, profileId);
        }

        [Route("trips/{tripid:int}/authorization")]
        [HttpGet]
        public BusinessTripInfo GetTripForAuthorization(int tripId)
        {
            return _businessTripService.GetTripForAuthorization(tripId);
        }

        [Route("trips/{tripid:int}/authorizationDetail")]
        [HttpGet]
        public BusinessTrip GetTripForAuthorizationDetail(int tripId)
        {
            return _businessTripService.GetTripForAuthorizationDetail(tripId);
        }
    }
}
