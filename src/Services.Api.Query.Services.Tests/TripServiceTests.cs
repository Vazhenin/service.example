using System;
using System.Collections.Generic;
using System.Linq;
using Dto = Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Data.Entities;
using Services.Api.Query.Services.Data.Entities.Journeys;
using Services.Api.Query.Services.Data.Entities.Services;
using Services.Api.Query.Services.Interfaces;
using AutoFixture;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Services.Api.Query.Services.Tests
{
    [TestFixture]
    public class TripServiceTests
    {
        private CabinetDbContext _cabinetDbContext;

        private TripService _target;

        private Mock<IMapper> _mapper;

        private Mock<IPrincipalResolver> _principalResolver;

        private Fixture _fixture;

        [SetUp]
        public void InitTest()
        {
            var options = new DbContextOptionsBuilder<CabinetDbContext>()
                .UseInMemoryDatabase("Test")
                .Options;

            _cabinetDbContext = new CabinetDbContext(options);

            _mapper = new Mock<IMapper>();

            _principalResolver = new Mock<IPrincipalResolver>();

            _target = new TripService(_cabinetDbContext, _mapper.Object, _principalResolver.Object);

            _fixture = new Fixture();
        }

        [Test]
        public void GetTripByNumber_NotFound_Null()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();

            var result = _target.GetTripByNumber(journeyNumber, tripNumber);

            Assert.Null(result);
        }

        [Test]
        public void GetTripByNumber_BusinessTrip_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();

            var trip = new BusinessTrip
            {
                Number = tripNumber,
                Journey = new Journey
                {
                    Number = journeyNumber
                }
            };

            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);
        }

        [Test]
        public void GetTripByNumber_CustomPropertyValues_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();

            var trip = new BusinessTrip
            {
                Number = tripNumber,
                Journey = new Journey
                {
                    Number = journeyNumber
                },
                CustomPropertyValues = new List<BusinessTripCustomPropertyValue>
                {
                    new BusinessTripCustomPropertyValue
                    {
                        NameRu = _fixture.Create<string>(),
                        ValueEn = _fixture.Create<string>()
                    }
                }
            };

            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);

            for (var i = 0; i < result.CustomPropertyValues.Count(); i++)
            {
                var resultValue = result.CustomPropertyValues.ElementAt(i);
                var expectedValue = trip.CustomPropertyValues.ElementAt(i);

                Assert.AreEqual(expectedValue.NameRu, resultValue.NameRu);
                Assert.AreEqual(expectedValue.ValueEn, resultValue.ValueEn);
            }
        }

        [Test]
        public void GetTripByNumber_BusinessTripExpenses_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();

            var trip = new BusinessTrip
            {
                Number = tripNumber,
                Journey = new Journey
                {
                    Number = journeyNumber
                },
                BusinessTripExpenses = new List<BusinessTripExpenses>
                {
                    new BusinessTripExpenses
                    {
                        ExpensesSnapshotNameEn = _fixture.Create<string>(),
                        ExpensesSnapshotNameRu = _fixture.Create<string>(),
                        Cost = _fixture.Create<decimal>(),
                    }
                }
            };

            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);

            for (var i = 0; i < result.BusinessTripExpenses.Count(); i++)
            {
                var resultValue = result.BusinessTripExpenses.ElementAt(i);
                var expectedValue = trip.BusinessTripExpenses.ElementAt(i);

                Assert.AreEqual(expectedValue.Cost, resultValue.Cost);
                Assert.AreEqual(expectedValue.ExpensesSnapshotNameEn, resultValue.ExpensesSnapshotNameEn);
                Assert.AreEqual(expectedValue.ExpensesSnapshotNameRu, resultValue.ExpensesSnapshotNameRu);
            }
        }

        [Test]
        public void GetTripByNumber_IsAnyDocuments_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var document = new Document { ServiceId = serviceId, DeletedAt = null };

            _cabinetDbContext.Documents.Add(document);
            _cabinetDbContext.SaveChanges();

            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.True(result.HasDocuments);
        }

        [Test]
        public void GetTripByNumber_NoServices_TripWithoutServices()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();

            var trip = new BusinessTrip { Number = tripNumber, Journey = new Journey { Number = journeyNumber } };

            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.True(!result.Services.Any());
        }

        [Test]
        public void GetTripByNumber_UnknownService_TripWithoutServices()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.Unknown };

            AddService(service);

            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.True(!result.Services.Any());
        }

        [Test]
        public void GetTripByNumber_AviaService_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.Avia };

            var departureFromAirportId = _fixture.Create<int>();
            var arrivalToAirportId = _fixture.Create<int>();
            var duration = _fixture.Create<long>();

            var segment1 = new AviaSegment
            {
                CabinClass = CabinClass.Economy,
                BaggageQuantity = 0
            };

            var segment2 = new AviaSegment
            {
                CabinClass = CabinClass.Economy,
                BaggageQuantity = 0
            };

            var aviaLeg1 = new AviaLeg
            {
                DepartureFromAirportId = departureFromAirportId,
                ArrivalToAirportId = arrivalToAirportId,
                Duration = duration,
                Segments = new List<AviaSegment> { segment1, segment2 }
            };

            var aviaLeg2 = new AviaLeg
            {
                DepartureFromAirportId = departureFromAirportId,
                ArrivalToAirportId = arrivalToAirportId,
                Duration = duration,
                Segments = new List<AviaSegment> { segment1, segment2 }
            };

            var aviaService = new AviaService
            {
                Id = serviceId,
                Legs = new List<AviaLeg> { aviaLeg1, aviaLeg2 }.ToArray()
            };

            service.AviaService = aviaService;

            AddService(service);


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);

            var firstService = result.Services.OfType<Dto.AviaServiceCard>().FirstOrDefault();

            Assert.NotNull(firstService);
            Assert.AreEqual(ServiceType.Avia, firstService?.Type);

            for (var i = 0; i < firstService.Legs.Length; i++)
            {
                var resultLeg = firstService.Legs.ElementAt(i);
                var expectedLeg = aviaService.Legs.ElementAt(i);

                Assert.AreEqual(expectedLeg.DepartureFromAirportId, resultLeg.DepartureFromAirportId);
                Assert.AreEqual(expectedLeg.ArrivalToAirportId, resultLeg.ArrivalToAirportId);
                Assert.AreEqual(expectedLeg.Segments.Count - 1, resultLeg.Transfers);
                Assert.AreEqual(expectedLeg.Duration, resultLeg.Duration);

                for (var j = 0; j < resultLeg.Segments.Count(); j++)
                {
                    var resultSegment = resultLeg.Segments.ElementAt(j);
                    var expectedSegment = expectedLeg.Segments.ElementAt(j);

                    Assert.AreEqual(expectedSegment.CabinClass, resultSegment.CabinClass);
                    Assert.AreEqual(expectedSegment.BaggageQuantity, resultSegment.BaggageQuantity);
                    Assert.AreEqual(expectedSegment.BaggageType, resultSegment.BaggageType);
                    Assert.AreEqual(expectedSegment.BaggageWeightMeasurement, resultSegment.BaggageWeightMeasurement);
                }
            }
        }

        [Test]
        public void GetTripByNumber_HotelService_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.Hotel };

            var actualCheckinAt = _fixture.Create<DateTime>();
            var actualCheckoutAt = _fixture.Create<DateTime>();
            var hotelId = _fixture.Create<int>();

            var hotelService = new HotelService
            {
                Id = serviceId,
                ActualCheckinAt = actualCheckinAt,
                ActualCheckoutAt = actualCheckoutAt,
                HotelId = hotelId,
                Meals = new List<HotelMeal>
                {
                    new HotelMeal
                    {
                        Code = _fixture.Create<string>(),
                        Name = _fixture.Create<string>()
                    }
                }
            };

            service.HotelService = hotelService;

            AddService(service);


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);

            var firstService = result.Services.OfType<Dto.HotelServiceCard>().FirstOrDefault();

            Assert.NotNull(firstService);
            Assert.AreEqual(ServiceType.Hotel, firstService.Type);

            Assert.AreEqual(actualCheckinAt, firstService.ActualCheckinAt);
            Assert.AreEqual(actualCheckoutAt, firstService.ActualCheckoutAt);
            Assert.AreEqual(hotelId, firstService.HotelId);
            Assert.AreEqual(hotelService.Meals.Count, firstService.Meals.Count());

            for (var i = 0; i < firstService.Meals.Count(); i++)
            {
                var firstServiceMeal = firstService.Meals.ElementAt(i);
                var expectedMeal = hotelService.Meals.ElementAt(i);

                Assert.AreEqual(expectedMeal.Code, firstServiceMeal.Code);
                Assert.AreEqual(expectedMeal.Name, firstServiceMeal.Name);
            }
        }

        [Test]
        public void GetTripByNumber_RailService_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.Rail };

            var railService = _fixture.Create<RailService>();

            service.RailService = railService;

            AddService(service);


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);
            Assert.True(result.Services.Any());

            var firstService = result.Services.OfType<Dto.RailServiceCard>().FirstOrDefault();

            Assert.AreEqual(ServiceType.Rail, firstService?.Type);

            Assert.AreEqual(railService.DepartureFromStationId, firstService?.DepartureFromStationId);
            Assert.AreEqual(railService.ArrivalToStationId, firstService?.ArrivalToStationId);
            Assert.AreEqual(railService.TrainCode, firstService?.TrainCode);
            Assert.AreEqual(railService.TrainName, firstService?.TrainName);
            Assert.AreEqual(railService.PublicTrainCode, firstService?.PublicTrainCode);
            Assert.AreEqual(railService.TrainCategory, firstService?.TrainCategory);
            Assert.AreEqual(railService.CarriageType, firstService?.CarriageType);
            Assert.AreEqual(railService.CarriageNumber, firstService?.CarriageNumber);
            Assert.AreEqual(railService.IsElectronicRegistrationAllowed, firstService?.IsElectronicRegistrationAllowed);
            Assert.AreEqual(railService.Duration, firstService?.Duration);
        }

        [Test]
        public void GetTripByNumber_AeroExpressService_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.AeroExpress };

            var aeroExpressService = _fixture.Create<AeroExpressService>();

            service.AeroExpressService = aeroExpressService;

            AddService(service);


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);
            Assert.True(result.Services.Any());

            var firstService = result.Services.OfType<Dto.AeroExpressServiceCard>().FirstOrDefault();

            Assert.AreEqual(ServiceType.AeroExpress, firstService?.Type);

            Assert.AreEqual(aeroExpressService.TariffClass, firstService?.TariffClass);
            Assert.AreEqual(TimeSpan.FromTicks(aeroExpressService.DepartureAt ?? 0), firstService?.DepartureAt);
            Assert.AreEqual(TimeSpan.FromTicks(aeroExpressService.ArrivalAt ?? 0), firstService?.ArrivalAt);
        }

        [Test]
        public void GetTripByNumber_TransferService_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var tripNumber = _fixture.Create<int>();
            var serviceId = _fixture.Create<int>();

            AddTrip(journeyNumber, tripNumber, serviceId);

            var service = new Service { Id = serviceId, Type = ServiceType.Transfer };

            var transferService = _fixture.Create<TransferService>();

            service.TransferService = transferService;

            AddService(service);


            var result = _target.GetTripByNumber(journeyNumber, tripNumber);


            Assert.NotNull(result);
            Assert.AreEqual(journeyNumber, result.JourneyNumber);
            Assert.AreEqual(tripNumber, result.Number);
            Assert.True(result.Services.Any());

            var firstService = result.Services.OfType<Dto.TransferServiceCard>().FirstOrDefault();

            Assert.AreEqual(ServiceType.Transfer, firstService?.Type);

            Assert.AreEqual(transferService.CarClassification, firstService?.CarClassification);
        }

        private void AddTrip(int journeyNumber, int tripNumber, int serviceId)
        {
            var tripId = _fixture.Create<int>();

            var trip = new BusinessTrip
            {
                Id = tripId,
                Number = tripNumber,
                Journey = new Journey
                {
                    Number = journeyNumber
                },
                BusinessTripServices = new List<BusinessTripService>
                {
                    new BusinessTripService
                    {
                        BusinessTripId = tripId,
                        ServiceId = serviceId
                    }
                }
            };

            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();
        }

        private void AddService(Service service)
        {
            _cabinetDbContext.Services.Add(service);
            _cabinetDbContext.SaveChanges();
        }
    }
}
