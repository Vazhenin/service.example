﻿using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Data.Entities.Journeys;
using Services.Api.Query.Services.Data.Entities.Services;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Mapping;
using AutoFixture;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Services.Api.Query.Model.Dto.Search;
using NUnit.Framework;

namespace Services.Api.Query.Services.Tests
{
    [TestFixture]
    public class JourneyServiceTests
    {
        private CabinetDbContext _cabinetDbContext;

        private JourneyService _target;

        private IMapper _mapper;

        private Fixture _fixture;

        private Mock<IPrincipalResolver> _principalResolver;

        [SetUp]
        public void InitTest()
        {
            var options = new DbContextOptionsBuilder<CabinetDbContext>()
                .UseInMemoryDatabase("Test")
                .Options;

            _cabinetDbContext = new CabinetDbContext(options);

            _mapper = new MapperConfiguration(x =>
            {
                x.AddProfile<MappingProfile>();
            }).CreateMapper();

            _principalResolver = new Mock<IPrincipalResolver>();

            _target = new JourneyService(_cabinetDbContext, _mapper, _principalResolver.Object);

            _fixture = new Fixture();
        }

        [Test]
        public void GetJourneyByNumber_Success()
        {
            int numberToFind = _fixture.Create<int>();

            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = numberToFind }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = numberToFind }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters() {
                JourneyNumber = numberToFind,
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(numberToFind, result.Single().Number);
            Assert.False(result.Single().BusinessTrips.Any(x => x.Number == numberToFind));
        }

        [Test]
        public void GetJourneyByTripNumber_Success()
        {
            int numberToFind = _fixture.Create<int>();

            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = numberToFind }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = numberToFind }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                TripNumber = numberToFind,
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.Contains(numberToFind, result.Single().BusinessTrips.Select(x => x.Number).ToList());
            Assert.AreNotEqual(numberToFind, result.Single().Number);
        }

        [Test]
        public void GetJourneyByJourneyTripNumber_Success()
        {
            int numberToFind = _fixture.Create<int>();

            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = numberToFind }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = numberToFind }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));
            AddTrip(new Journey() { Number = GetInt(numberToFind) }, new BusinessTrip() { Number = GetInt(numberToFind) }, GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                JourneyOrTripNumber = numberToFind,
                PageSize = 10
            });

            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void GetJourneyByCompanyIds_Success()
        {
            int numberToFind = _fixture.Create<int>();
            AddTrip(
                new Journey() {  },
                new BusinessTrip() { CompanyId = GetInt(numberToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() {  },
                new BusinessTrip() { CompanyId = numberToFind },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { CompanyId = GetInt(numberToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { CompanyId = GetInt(numberToFind) },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                CompanyIds = numberToFind.ToString(),
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.Contains(numberToFind, result.Single().BusinessTrips.Select(x => x.CompanyId).ToList());
        }

        [Test]
        public void GetJourneyByProfileIds_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int principalIdToFind = _fixture.Create<int>();
            var principalToFind = _fixture.Build<Model.Dto.Principal>().With(pr => pr.Id, principalIdToFind).Create();
            _principalResolver.Setup(pr => pr.GetPrincipalByProfileId(It.IsAny<int>())).Returns(_fixture.Create<Model.Dto.Principal>());
            _principalResolver.Setup(pr => pr.GetPrincipalByProfileId(numberToFind)).Returns(principalToFind);
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind), CreatedByPrincipalId = GetInt(principalIdToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind), CreatedByPrincipalId = GetInt(principalIdToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = numberToFind, CreatedByPrincipalId = principalIdToFind },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind), CreatedByPrincipalId = GetInt(principalIdToFind) },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                ProfileIds = numberToFind.ToString(),
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.Contains(numberToFind, result.Single().BusinessTrips.Select(x => x.ProfileId).ToList());
        }

        [Test]
        public void GetJourneyByTravallerProfileIds_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int principalIdToFind = _fixture.Create<int>();
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = numberToFind },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { },
                new BusinessTrip() { ProfileId = GetInt(numberToFind) },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                TravellerProfileIds = numberToFind.ToString(),
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.Contains(numberToFind, result.Single().BusinessTrips.Select(x => x.ProfileId).ToList());
        }

        [Test]
        public void GetJourneyByDatePeriod_Success()
        {
            var date = DateTime.Now;

            int numberToFind = _fixture.Create<int>();
            AddTrip(
                new Journey() { StartsOn = date.AddDays(-10), EndsOn = date, Id = GetInt(numberToFind) }, // начинается до периода
                new BusinessTrip() {  },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date, EndsOn = date.AddDays(1), Id = numberToFind }, // начинается и заканчивается в рамках периода
                new BusinessTrip() {  },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date.AddMinutes(1), EndsOn = date.AddDays(10), Id = GetInt(numberToFind) }, //заканчивается после периода
                new BusinessTrip() {  },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date.AddDays(-100), EndsOn = date.AddDays(-90), Id = GetInt(numberToFind) }, // начинается и заканчивается до периода
                new BusinessTrip() {  },
                GetInt(numberToFind));

            AddTrip(
                new Journey() { StartsOn = date.AddDays(90), EndsOn = date.AddDays(100), Id = GetInt(numberToFind) }, // начинается и заканчивается после периода
                new BusinessTrip() { },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                DateFrom = date.AddDays(-1),
                DateTo = date.AddDays(1),
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(numberToFind, result.Single().Id);
        }

        [Test]
        public void GetJourneyByStartDatePeriod_Success()
        {
            var date = DateTime.Now;

            int numberToFind = _fixture.Create<int>();
            AddTrip(
                new Journey() { StartsOn = date.AddDays(-10), EndsOn = date, Id = GetInt(numberToFind) }, // начинается до периода
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date.AddDays(-2), EndsOn = date.AddDays(2), Id = numberToFind }, // начинается и заканчивается в рамках периода
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date.AddMinutes(1), EndsOn = date.AddDays(10), Id = GetInt(numberToFind) }, //заканчивается после периода
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { StartsOn = date.AddDays(-100), EndsOn = date.AddDays(-90), Id = GetInt(numberToFind) }, // начинается и заканчивается до периода
                new BusinessTrip() { },
                GetInt(numberToFind));

            AddTrip(
                new Journey() { StartsOn = date.AddDays(90), EndsOn = date.AddDays(100), Id = GetInt(numberToFind) }, // начинается и заканчивается после периода
                new BusinessTrip() { },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                StartsBeforeDate = date.AddDays(-1),
                EndsAfterDate = date.AddDays(1),
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(numberToFind, result.Single().Id);
        }

        [Test]
        public void GetJourneyByTravelToCityId_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int id = _fixture.Create<int>();
            AddTrip(
                new Journey() { Id = GetInt(id), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = id, JourneyToCityId = numberToFind },
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = GetInt(id), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = GetInt(id), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { },
                GetInt(numberToFind));

            AddTrip(
                new Journey() { Id = GetInt(id), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                TravelToCityId = numberToFind,
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(numberToFind, result.Single().JourneyToCityId);
            Assert.AreEqual(id, result.Single().Id);
        }

        [Test]
        public void GetJourneyByTravelFromCityId_Success()
        {
            var journeyNumber = _fixture.Create<int>();
            var date = DateTime.Now;

            int numberToFind = _fixture.Create<int>();
            int id = _fixture.Create<int>();
            AddTrip(
                new Journey() {},
                new BusinessTrip() { TravelFromCityId = GetInt(numberToFind), Id = GetInt(id) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() {},
                new BusinessTrip() { TravelFromCityId = GetInt(numberToFind), Id = GetInt(id) },
                GetInt(numberToFind));
            AddTrip(
                new Journey() {},
                new BusinessTrip() { TravelFromCityId = numberToFind, Id = id },
                GetInt(numberToFind));
            AddTrip(
                new Journey() {},
                new BusinessTrip() { TravelFromCityId = GetInt(numberToFind), Id = GetInt(id) },
                GetInt(numberToFind));

            AddTrip(
                new Journey() {},
                new BusinessTrip() { TravelFromCityId = GetInt(numberToFind), Id = GetInt(id) },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                TravelFromCityId = numberToFind,
                PageSize = 10
            });

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(numberToFind, result.Single().BusinessTrips.Single().TravelFromCityId);
            Assert.AreEqual(id, result.Single().BusinessTrips.Single().Id);
        }

        [Test]
        public void GetJourneyByOrderNumberUsingSearchMethod_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int id = _fixture.Create<int>();
            var servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId }, new Order { Number = numberToFind });
            AddTrip(
                new Journey() { Id = id },
                new BusinessTrip() { },
                servId); // правильный сервис.
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);
 
            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                OrderNumber = numberToFind,
                PageSize = 10
            });
            
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(id, result.Single().Id);
        }

        [Test]
        public void GetJourneyByServiceType_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int id_hotel = _fixture.Create<int>();
            int id_rail = _fixture.Create<int>();
            var servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId, Type = Model.Enum.ServiceType.Avia }, new Order { });
            AddTrip(
                new Journey() { Id = GetInt(new[] { id_hotel, id_rail }) },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId, Type = Model.Enum.ServiceType.Rail }, new Order { });
            AddTrip(
                new Journey() { Id = id_rail },
                new BusinessTrip() {},
                servId); // правильный сервис.
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId, Type = Model.Enum.ServiceType.AeroExpress }, new Order { });
            AddTrip(
                new Journey() { Id = GetInt(new[] { id_hotel, id_rail }) },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId, Type = Model.Enum.ServiceType.Hotel }, new Order { });
            AddTrip(
                new Journey() { Id = id_hotel },
                new BusinessTrip() { },
                servId); // правильный сервис.

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                ServiceType = new[] { Model.Enum.ServiceType.Hotel, Model.Enum.ServiceType.Rail },
                PageSize = 10
            });

            Assert.AreEqual(2, result.Count());
            Assert.True(result.Any(x => x.Id == id_rail));
            Assert.True(result.Any(x => x.Id == id_hotel));
        }

        [Test]
        [TestCaseSource(nameof(TestServiceDataGenerator))]
        public void GetJourneyByServiceState_Success(Service one, Service two, Service three, Service four, ServiceState expectedValue, int expectedCount)
        {
            int id = _fixture.Create<int>();
            one.Id = id;
            int servId = id;
            AddService(one, new Order { }); // правильный сервис.
            AddTrip(new Journey() { Id = id }, new BusinessTrip() { }, servId);
            servId = GetInt(id);
            two.Id = servId;
            AddService(two, new Order { });
            AddTrip(new Journey() { Id = GetInt(id) }, new BusinessTrip() { }, servId);
            servId = GetInt(id);
            three.Id = servId;
            AddService(three, new Order { });
            AddTrip(new Journey() { Id = GetInt(id) }, new BusinessTrip() { }, servId);
            servId = GetInt(id);
            four.Id = servId;
            AddService(four, new Order { });
            AddTrip(new Journey() { Id = GetInt(id) }, new BusinessTrip() { }, servId);
            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                ServiceState = new[] { expectedValue },
                PageSize = 10
            });
            Assert.AreEqual(expectedCount, result.Count());
        }

        [Test]
        public void GetJourneyByStatusFilter_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int id1 = _fixture.Create<int>();
            int id2 = _fixture.Create<int>();
            AddTrip(
                new Journey() { Id = GetInt(new[] { id1, id2 }), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { Status = TripStatus.Active },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = GetInt(new[] { id1, id2 }), JourneyToCityId = numberToFind },
                new BusinessTrip() { Status = TripStatus.Active },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = id1, JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { Status = TripStatus.Complete },
                GetInt(numberToFind));
            AddTrip(
                new Journey() { Id = GetInt(new[] { id1, id2 }), JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { Status = TripStatus.Draft },
                GetInt(numberToFind));

            AddTrip(
                new Journey() { Id = id2, JourneyToCityId = GetInt(numberToFind) },
                new BusinessTrip() { Status = TripStatus.Closed },
                GetInt(numberToFind));

            var result = _target.SearchJourneysPaged(new JourneyParameters()
            {
                StatusFilter = new[] { TripStatus.Active, TripStatus.Draft },
                PageSize = 10
            });

            Assert.AreEqual(3, result.Count());
            Assert.False(result.Any(j => (new[] { id1, id2 }).Contains(j.Id)));
        }


        [Test]
        public void GetJourneyByOrderNumber_Success()
        {
            int numberToFind = _fixture.Create<int>();
            int id = _fixture.Create<int>();
            var servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = servId }, new Order { Number = numberToFind });
            AddTrip(
                new Journey() { Id = id },
                new BusinessTrip() { },
                servId); // правильный сервис.
            servId = GetInt(numberToFind);
            AddService(new Service() { Id = GetInt(id) }, new Order { Number = GetInt(numberToFind) });
            AddTrip(
                new Journey() { },
                new BusinessTrip() { },
                servId);

            var result = _target.GetJourneyByOrderNumber(numberToFind);
            Assert.AreNotEqual(result, null);
            Assert.AreEqual(id, result.Id);
        }

        private int GetInt(int[] exceptValues)
        {
            var generator = _fixture.Create<Generator<int>>();
            return generator.FirstOrDefault(x => !exceptValues.Contains(x));
        }

        private int GetInt(int exceptValue)
        {
            return GetInt(new[] { exceptValue });
        }

        private void AddTrip(Journey journey, BusinessTrip businessTrip, int serviceId)
        {
            var tripid = businessTrip.Id == 0 ? _fixture.Create<int>() : businessTrip.Id;
            var trip = new BusinessTrip
            {
                Id = tripid,
                Number = businessTrip.Number == 0 ? _fixture.Create<int>() : businessTrip.Number,
                CompanyId = businessTrip.CompanyId == 0 ? _fixture.Create<int>() : businessTrip.CompanyId,
                ProfileId = businessTrip.ProfileId == 0 ? _fixture.Create<int>() : businessTrip.ProfileId,
                TravelFromCityId = businessTrip.TravelFromCityId == 0 ? _fixture.Create<int>() : businessTrip.TravelFromCityId,
                Status = businessTrip.Status,
                Journey = new Journey
                {
                    Id = journey.Id == 0 ? _fixture.Create<int>() : journey.Id,
                    Number = journey.Number == 0 ? _fixture.Create<int>() : journey.Number,
                    StartsOn = journey.StartsOn.Year == 1 ? DateTime.Now.AddDays(100) : journey.StartsOn,
                    EndsOn = journey.EndsOn.Year == 1 ? DateTime.Now.AddDays(200) : journey.EndsOn,
                    JourneyToCityId = journey.JourneyToCityId == 0 ? _fixture.Create<int>() : journey.JourneyToCityId,
                },
                BusinessTripServices = new List<BusinessTripService>
                {
                    new BusinessTripService
                    {
                        BusinessTripId = tripid,
                        ServiceId = serviceId
                    }
                },
                CreatedByPrincipalId = businessTrip.CreatedByPrincipalId == 0 ? _fixture.Create<int>() : businessTrip.CreatedByPrincipalId,
            };
            _cabinetDbContext.BusinessTrips.Add(trip);
            _cabinetDbContext.SaveChanges();
        }

        private int AddService(Service service, Order order)
        {
            var serv = new Service()
            {
                Id = service.Id == 0 ? _fixture.Create<int>() : service.Id,
                Type = service.Type,
                ReservationStatus = service.ReservationStatus,
                ExecutionStatus = service.ExecutionStatus,
                RefundStatus = service.RefundStatus,
                ServicingStatus = service.ServicingStatus,
                Status = service.Status,
                Order = new Order()
                {
                    Number = order.Number == 0 ? _fixture.Create<int>() : order.Number,
                }
            };
            _cabinetDbContext.Services.Add(serv);
            _cabinetDbContext.SaveChanges();
            return serv.Id;
        }

        private static IEnumerable TestServiceDataGenerator()
        {
            yield return GetUnknownList();
            yield return GetRefundingList();
            yield return GetExecutionList();
            yield return GetReservationList();
            yield return GetServicingList();
        }

        /// <summary>
        /// Список услуг для неизвестного состояния.
        /// </summary>
        /// <returns>Список услуг.</returns>
        private static TestCaseData GetUnknownList()
        {
            var res = new List<object>
            {
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.Pending,
                    ServicingStatus = ServicingStatus.Undefined,
                    ExecutionStatus = ExecutionStatus.Pending
                },
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.InProgress,
                    ServicingStatus = ServicingStatus.Undefined,
                    ExecutionStatus = ExecutionStatus.Pending
                },
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.Pending,
                    ServicingStatus = ServicingStatus.Pending,
                    ExecutionStatus = ExecutionStatus.Pending
                },
                new Service()
                {
                    Status = ServiceStatus.Archived,
                    ReservationStatus = ReservationStatus.Pending,
                    ServicingStatus = ServicingStatus.Undefined,
                    ExecutionStatus = ExecutionStatus.Rejected
                },
                ServiceState.Unknown,
                1
            };
            return new TestCaseData(res.ToArray());
        }

        /// <summary>
        /// Список услуг для возвратов.
        /// </summary>
        /// <returns>Список услуг.</returns>
        private static TestCaseData GetRefundingList()
        {
            var res = new List<object>
            {
                new Service()
                {
                    RefundStatus = RefundStatus.Succeeded
                },
                new Service()
                {
                    RefundStatus = RefundStatus.Failed
                },
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Cancelled
                },
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Succeeded
                },
                ServiceState.Refunding,
                2
            };
            return new TestCaseData(res.ToArray());
        }

        /// <summary>
        /// Список услуг для оформлено.
        /// </summary>
        /// <returns>Список услуг.</returns>
        private static TestCaseData GetExecutionList()
        {
            var res = new List<object>
            {
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.Completed
                },
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Pending,
                    ServicingStatus = ServicingStatus.Completed
                },
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.ResponseWaiting
                },
                new Service()
                {
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.InProgress
                },
                ServiceState.Execution,
                1
            };
            return new TestCaseData(res.ToArray());
        }

        /// <summary>
        /// Список услуг для забронировано.
        /// </summary>
        /// <returns>Список услуг.</returns>
        private static TestCaseData GetReservationList()
        {
            var res = new List<object>
            {
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.Succeeded,
                    RefundStatus = RefundStatus.Pending,
                    ExecutionStatus = ExecutionStatus.Pending,
                    ServicingStatus = ServicingStatus.Completed
                },
                new Service()
                {
                    Status = ServiceStatus.Archived,
                    ReservationStatus = ReservationStatus.InProgress,
                    RefundStatus = RefundStatus.Pending,
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.Completed
                },
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.Rejected,
                    RefundStatus = RefundStatus.Pending,
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.Completed
                },
                new Service()
                {
                    Status = ServiceStatus.Draft,
                    ReservationStatus = ReservationStatus.InProgress,
                    RefundStatus = RefundStatus.Failed,
                    ExecutionStatus = ExecutionStatus.Succeeded,
                    ServicingStatus = ServicingStatus.Completed
                },
                ServiceState.Reservation,
                1
            };
            return new TestCaseData(res.ToArray());
        }

        /// <summary>
        /// Список услуг для обслуживается.
        /// </summary>
        /// <returns>Список услуг.</returns>
        private static TestCaseData GetServicingList()
        {
            var res = new List<object>
            {
                new Service()
                {
                    RefundStatus = RefundStatus.Failed,
                    ServicingStatus = ServicingStatus.InProgress
                },
                new Service()
                {
                    RefundStatus = RefundStatus.Undefined,
                    ServicingStatus = ServicingStatus.Pending
                },
                new Service()
                {
                    RefundStatus = RefundStatus.Pending,
                    ServicingStatus = ServicingStatus.InProgress
                },
                new Service()
                {
                    RefundStatus = RefundStatus.Succeeded,
                    ServicingStatus = ServicingStatus.InProgress
                },
                ServiceState.Servicing,
                2
            };
            return new TestCaseData(res.ToArray());
        }
    }
}