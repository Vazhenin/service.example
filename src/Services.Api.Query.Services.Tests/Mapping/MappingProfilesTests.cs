﻿using Services.Api.Query.Services.Mapping;
using AutoMapper;
using NUnit.Framework;

namespace Services.Api.Query.Services.Tests.Mapping
{
    [TestFixture]
    public class MappingProfilesTests
    {
        private IMapper _target;

        [Test]
        public void MappingProfile_Validate()
        {
            _target = new MapperConfiguration(x =>
            {
                x.AddProfile<MappingProfile>();
            }).CreateMapper();

            _target.ConfigurationProvider.AssertConfigurationIsValid();
        }

        [Test]
        public void ProjectionProfile_Validate()
        {
            _target = new MapperConfiguration(x =>
            {
                x.AddProfile<ProjectionProfile>();
            }).CreateMapper();

            _target.ConfigurationProvider.AssertConfigurationIsValid();
        }
    }
}
