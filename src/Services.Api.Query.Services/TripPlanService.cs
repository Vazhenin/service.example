﻿using System;
using System.Collections.Generic;
using System.Linq;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Model.Search;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Data.Extensions;
using Services.Api.Query.Services.Data.Extensions.Queries;
using Services.Api.Query.Services.Interfaces;

namespace Services.Api.Query.Services
{
    public class TripPlanService : ITripPlanService
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public TripPlanService(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public ICollection<ServiceCard> GetTripPlan(TripPlanParameters parameters)
        {
            var query = _cabinetDbContext.GetServicesQuery()
                .Where(s =>
                s.ExecutionStatus == ExecutionStatus.Succeeded ||
                s.ExecutionStatus == ExecutionStatus.Cancelled ||
                s.RefundStatus == RefundStatus.Succeeded);

            if (parameters.TravellerProfileId.HasValue)
            {
                query = query.Where(s => _cabinetDbContext.Traveller.Any(t => t.ProfileId == parameters.TravellerProfileId && t.ServiceId == s.Id));
            }

            if (parameters.MinimumEndOn.HasValue)
                query = query.Where(s => s.EndOn.Value >= parameters.MinimumEndOn);
            if (parameters.MaximumEndOn.HasValue)
                query = query.Where(s => s.EndOn.Value <= parameters.MaximumEndOn);

            if (parameters.Version.HasValue)
            {
                var bitVersion = BitConverter.GetBytes(parameters.Version.Value).Reverse().ToArray();
                query = query.Where(s => bitVersion.Compare(s.Version) < 0);
            }

            query = query.OrderBy(q => q.StartOn ?? q.CreatedAt);

            var dbServices = query.Skip(parameters.PageNumber * parameters.PageSize).Take(parameters.PageSize).ToList();

            var services = new List<ServiceCard>();

            foreach (var dbService in dbServices)
            {
                var service = dbService.ConvertToServiceLite();

                if (service != null)
                    services.Add(service);
            }

            return services;
        }
    }
}
