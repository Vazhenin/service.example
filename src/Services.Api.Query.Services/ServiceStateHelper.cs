﻿using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data.Entities.Services;
using System;
using System.Linq;
namespace Services.Api.Query.Services
{

    /// <summary>
    /// Хелпер для состояний услуги.
    /// </summary>
    public static class ServiceStateHelper
    {
        /// <summary>
        /// Получение экспрешена для состояний.
        /// </summary>
        /// <param name="states">Список состояний.</param>
        /// <returns>Экспрешен для состояний.</returns>
        public static Func<Service, bool> GetServiceExpression(ServiceState[] states)
        {
            if (states.Any())
            {
                Func<Service, bool> result = GetServiceExpression(states[0]);
                for (int i = 1; i < states.Length; i++)
                {
                    result = x => result(x) || GetServiceExpression(states[i])(x);
                }
                return result;
            }
            return (x => true);
        }

        /// <summary>
        /// Получение экспрешена для состояния.
        /// </summary>
        /// <param name="state">Состояние.</param>
        /// <returns>Экспрешен для состояния.</returns>
        public static Func<Service, bool> GetServiceExpression(ServiceState state)
        {
            switch (state)
            {
                case ServiceState.Unknown:
                    return x => (x.Status == ServiceStatus.Draft) &&
                            (x.ReservationStatus == ReservationStatus.Pending) &&
                            (x.ServicingStatus == ServicingStatus.Undefined) &&
                            (x.ExecutionStatus == ExecutionStatus.Pending);
                case ServiceState.Reservation:
                    return x => (x.Status == ServiceStatus.Draft) &&
                            (x.ReservationStatus == ReservationStatus.Succeeded) &&
                            (x.ServicingStatus == ServicingStatus.Completed) &&
                            (x.RefundStatus == RefundStatus.Pending) &&
                            (x.ExecutionStatus == ExecutionStatus.Pending);
                case ServiceState.Refunding:
                    return x => (x.RefundStatus == RefundStatus.Succeeded) ||
                        (x.ExecutionStatus == ExecutionStatus.Cancelled);
                case ServiceState.Servicing:
                    return x => ((x.ServicingStatus == ServicingStatus.InProgress) &&
                             (x.RefundStatus == RefundStatus.Undefined)) ||
                             ((x.ServicingStatus == ServicingStatus.InProgress) &&
                             (x.RefundStatus == RefundStatus.Pending)) ||
                             ((x.ServicingStatus == ServicingStatus.Pending) &&
                             (x.RefundStatus == RefundStatus.Undefined)) ||
                             ((x.ServicingStatus == ServicingStatus.Pending) &&
                             (x.RefundStatus == RefundStatus.Pending));
                case ServiceState.Execution:
                    return x => (x.ExecutionStatus == ExecutionStatus.Succeeded) &&
                              (x.ServicingStatus == ServicingStatus.Completed);
            }
            return x => true;
        }
    }
}