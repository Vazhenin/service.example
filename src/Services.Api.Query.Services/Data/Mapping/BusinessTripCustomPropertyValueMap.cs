﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Api.Query.Services.Data.Entities.Journeys;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class BusinessTripCustomPropertyValueMap : DbEntityConfiguration<BusinessTripCustomPropertyValue>
    {
        public BusinessTripCustomPropertyValueMap() : base(tableName: "BusinessTripCustomPropertyValue")
        {
        }
        public override void Configure(EntityTypeBuilder<BusinessTripCustomPropertyValue> entity)
        {
            base.Configure(entity);
            entity.Property(v => v.ValueEn).HasColumnName("ValueEn");
            entity.Property(v => v.ValueRu).HasColumnName("ValueRu");
            entity.Property(v => v.NameEn).HasColumnName("CustomPropertyNameEn");
            entity.Property(v => v.NameRu).HasColumnName("CustomPropertyNameRu");
        }
    }
}
