﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Api.Query.Services.Data.Entities.Journeys;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class BusinessTripExpensesMap : DbEntityConfiguration<BusinessTripExpenses>
    {
        public BusinessTripExpensesMap() : base(tableName: "BusinessTripExpenses")
        {
        }

        public override void Configure(EntityTypeBuilder<BusinessTripExpenses> entity)
        {
            base.Configure(entity);
            entity.Property(v => v.BusinessTripId).HasColumnName("BusinessTripID");
            entity.Property(v => v.Cost).HasColumnName("Cost");
            entity.Property(v => v.ExpensesSnapshotNameEn).HasColumnName("ExpensesSnapshotNameEn");
            entity.Property(v => v.ExpensesSnapshotNameRu).HasColumnName("ExpensesSnapshotNameRu");
        }
    }
}
