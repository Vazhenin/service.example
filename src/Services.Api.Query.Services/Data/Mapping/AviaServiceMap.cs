﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class AviaServiceMap : DbEntityConfiguration<AviaService>
    {
        public AviaServiceMap() : base(tableName: "ServiceAvia")
        {
        }
        public override void Configure(EntityTypeBuilder<AviaService> entity)
        {
            base.Configure(entity);
            entity.HasMany(t => t.Legs).WithOne().HasForeignKey(v => v.ServiceId);
        }
    }
}
