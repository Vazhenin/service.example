﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class RailServiceMap : DbEntityConfiguration<RailService>
    {
        public RailServiceMap() : base(tableName: "ServiceRail")
        {
        }

        public override void Configure(EntityTypeBuilder<RailService> entity)
        {
            base.Configure(entity);
            entity.Property(s => s.CarriageNumber).HasColumnName("CarriageNum");
        }
    }
}