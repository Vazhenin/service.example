﻿using Services.Api.Query.Services.Data.Entities;
using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class TravellerMap : DbEntityConfiguration<Traveller>
    {
        public TravellerMap() : base(tableName: "Traveller")
        {
        }

        public override void Configure(EntityTypeBuilder<Traveller> entity)
        {
            base.Configure(entity);
        }
    }
}
