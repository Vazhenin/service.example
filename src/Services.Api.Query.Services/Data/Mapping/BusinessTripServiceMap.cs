﻿using Services.Api.Query.Services.Data.Entities.Journeys;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class BusinessTripServiceMap : DbEntityConfiguration<BusinessTripService>
    {
        public BusinessTripServiceMap() : base(tableName: "BusinessTripService")
        {
        }
        public override void Configure(EntityTypeBuilder<BusinessTripService> entity)
        {
            base.Configure(entity);
        }
    }
}