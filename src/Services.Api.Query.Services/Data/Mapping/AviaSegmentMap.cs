﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class AviaSegmentMap : DbEntityConfiguration<AviaSegment>
    {
        public AviaSegmentMap() : base(tableName: "AviaSegment")
        {
        }
        public override void Configure(EntityTypeBuilder<AviaSegment> entity)
        {
            base.Configure(entity);
            entity.Property(s => s.LegId).HasColumnName("AviaLegID");
            entity.Property(s => s.CabinClass).HasColumnName("ClassOfService");
            entity.Property(s => s.DepartureFromAirportId).HasColumnName("DepartureFromAirportID");
            entity.Property(s => s.ArrivalToAirportId).HasColumnName("ArrivalToAirportID");
            entity.Property(t => t.CabinClass).HasColumnName("ClassOfService");
            entity.Property(t => t.CabinClassCode).HasColumnName("CabinClass");
            entity.Property(t => t.ClassOfService).HasColumnName("Class");
        }
    }
}
