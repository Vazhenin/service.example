﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class OrderMap : DbEntityConfiguration<Order>
    {
        public OrderMap() : base(tableName: "Order")
        {
        }
        public override void Configure(EntityTypeBuilder<Order> entity)
        {
            base.Configure(entity);
        }
    }
}
