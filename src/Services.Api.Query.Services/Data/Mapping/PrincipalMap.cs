﻿using Services.Api.Query.Services.Data.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class PrincipalMap : DbEntityConfiguration<Principal>
    {
        public PrincipalMap() : base(tableName: "Principal")
        {
        }

        public override void Configure(EntityTypeBuilder<Principal> entity)
        {
            base.Configure(entity);
        }
    }
}