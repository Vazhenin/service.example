﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Api.Query.Services.Data.Entities;
namespace Services.Api.Query.Services.Data.Mapping
{

    public class PostMap : DbEntityConfiguration<Post>
    {
        public PostMap() : base(tableName: "Post")
        {
        }

        public override void Configure(EntityTypeBuilder<Post> entity)
        {
            base.Configure(entity);

            entity.HasOne(t => t.BusinessTtip).WithMany(t => t.Posts).HasForeignKey(t => t.BusinessTripId);
            entity.HasMany(t => t.Attachments).WithOne().HasForeignKey("PostID");
            entity.HasMany(t => t.Receipts).WithOne().HasForeignKey("PostID");
        }
    }
}