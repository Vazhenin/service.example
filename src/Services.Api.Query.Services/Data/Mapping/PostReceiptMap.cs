﻿using Services.Api.Query.Services.Data.Entities;
namespace Services.Api.Query.Services.Data.Mapping
{

    /// <summary>
    /// Маппинг таблички с полученными сообщениями.
    /// </summary>
    public class PostReceiptMap : DbEntityConfiguration<PostReceipt>
    {
        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public PostReceiptMap() : base(tableName: "PostReceipt")
        {
        }
    }
}