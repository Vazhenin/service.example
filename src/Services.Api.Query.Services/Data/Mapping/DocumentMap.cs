﻿using Services.Api.Query.Services.Data.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class DocumentMap : DbEntityConfiguration<Document>
    {
        public DocumentMap() : base(tableName: "Document")
        {
        }
        public override void Configure(EntityTypeBuilder<Document> entity)
        {
            base.Configure(entity);

            entity.HasMany(s => s.Attachments).WithOne();
        }
    }
}