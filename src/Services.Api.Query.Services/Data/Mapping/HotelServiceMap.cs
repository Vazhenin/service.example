﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class HotelServiceMap : DbEntityConfiguration<HotelService>
    {
        public HotelServiceMap() : base("ServiceHotel")
        {
        }
        public override void Configure(EntityTypeBuilder<HotelService> entity)
        {
            base.Configure(entity);
            entity.Property(s => s.HotelId).HasColumnName("HotelID");
            entity.Property(t => t.IsPayOnSite).HasColumnName("AsimIsPayOnSite"); ;

            entity.HasMany(t => t.Meals).WithOne().HasForeignKey(v => v.ServiceId);
        }
    }
}
