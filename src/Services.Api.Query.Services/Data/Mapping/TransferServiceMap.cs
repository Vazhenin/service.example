﻿using Services.Api.Query.Services.Data.Entities.Services;
using Services.Api.Query.Services.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class TransferServiceMap : DbEntityConfiguration<TransferService>
    {
        public TransferServiceMap() : base(tableName: "ServiceTransfer")
        {
        }
        public override void Configure(EntityTypeBuilder<TransferService> entity)
        {
            base.Configure(entity);
            entity.Property(t => t.CarClassification).HasColumnName("CarClassificationID");
        }
    }

}
