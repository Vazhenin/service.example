﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class ServiceMap : DbEntityConfiguration<Service>
    {
        public ServiceMap() : base(tableName: "Service")
        {
        }
        public override void Configure(EntityTypeBuilder<Service> entity)
        {
            base.Configure(entity);
            entity.Property(t => t.Type).HasColumnName("Type");
            entity.Property(t => t.TariffTotal).HasColumnName("TariffTotal").HasColumnType("decimal(18, 2)");

            entity.HasOne(s => s.Order).WithOne().HasForeignKey<Service>(s => s.OrderId);
            entity.HasOne(s => s.AviaService).WithOne().HasForeignKey<AviaService>(s => s.Id);
            entity.HasOne(s => s.HotelService).WithOne().HasForeignKey<HotelService>(s => s.Id);
            entity.HasOne(s => s.RailService).WithOne().HasForeignKey<RailService>(s => s.Id);
            entity.HasOne(s => s.TransferService).WithOne().HasForeignKey<TransferService>(s => s.Id);
            entity.HasOne(s => s.AeroExpressService).WithOne().HasForeignKey<AeroExpressService>(s => s.Id);
        }
    }
}
