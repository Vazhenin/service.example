﻿using Services.Api.Query.Services.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace Services.Api.Query.Services.Data.Mapping
{

    public abstract class DbEntityConfiguration<T> where T : BaseEntity
    {
        /// <summary>
        /// Имя таблицы для маппинга.
        /// </summary>
        private string tableName = null;

        protected DbEntityConfiguration(string tableName)
        {
            this.tableName = tableName;
        }

        /// <summary>
        /// Базовая конфигурация конфигурацию.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        public virtual void Configure(EntityTypeBuilder<T> entity)
        {
            entity.ToTable(tableName);
            entity.HasKey(s => s.Id).HasName("ID");
        }
    }
}
