﻿using Services.Api.Query.Services.Data.Entities.Journeys;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class JourneyMap : DbEntityConfiguration<Journey>
    {
        public JourneyMap() : base(tableName: "Journey")
        {
        }
        public override void Configure(EntityTypeBuilder<Journey> entity)
        {
            base.Configure(entity);
            entity.HasMany(j => j.BusinessTrips).WithOne(t => t.Journey).HasForeignKey(t => t.JourneyId);
        }
    }
}