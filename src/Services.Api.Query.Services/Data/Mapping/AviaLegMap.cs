﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class AviaLegMap : DbEntityConfiguration<AviaLeg>
    {
        public AviaLegMap() : base(tableName: "AviaLeg")
        {
        }

        public override void Configure(EntityTypeBuilder<AviaLeg> entity)
        {
            base.Configure(entity);
            entity.Property(s => s.ServiceId).HasColumnName("ServiceAviaID");
            entity.HasMany(t => t.Segments).WithOne().HasForeignKey(v => v.LegId);
        }
    }
}