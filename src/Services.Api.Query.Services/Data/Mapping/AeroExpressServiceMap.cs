﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Api.Query.Services.Data.Entities.Services;
namespace Services.Api.Query.Services.Data.Mapping
{

    public class AeroExpressServiceMap : DbEntityConfiguration<AeroExpressService>
    {
        public AeroExpressServiceMap() : base(tableName: "ServiceAeroExpress")
        {
        }

        public override void Configure(EntityTypeBuilder<AeroExpressService> entity)
        {
            base.Configure(entity);
        }
    }
}