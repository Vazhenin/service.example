﻿using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class HotelMealMap : DbEntityConfiguration<HotelMeal>
    {
        public HotelMealMap() : base(tableName: "ServiceHotelMeal")
        {
        }
        public override void Configure(EntityTypeBuilder<HotelMeal> entity)
        {
            base.Configure(entity);
            entity.Property(t => t.Code).HasMaxLength(400);
            entity.Property(t => t.Name).HasMaxLength(400);
            entity.Property(t => t.ServiceId).HasColumnName("ServiceHotelID");
        }
    }
}
