﻿using Services.Api.Query.Services.Data.Entities.Journeys;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace Services.Api.Query.Services.Data.Mapping
{

    public class BusinessTripMap : DbEntityConfiguration<BusinessTrip>
    {
        public BusinessTripMap() : base(tableName: "BusinessTrip")
        {
        }
        public override void Configure(EntityTypeBuilder<BusinessTrip> entity)
        {
            base.Configure(entity);
            entity.HasMany(t => t.CustomPropertyValues).WithOne(v => v.BusinessTrip).HasForeignKey(v => v.BusinessTripId);
            entity.HasMany(t => t.BusinessTripExpenses).WithOne(v => v.BusinessTrip).HasForeignKey(v => v.BusinessTripId);
            entity.HasMany(t => t.Posts).WithOne(t => t.BusinessTtip).HasForeignKey(s => s.BusinessTripId);
            entity.Ignore(t => t.IsAnyDocuments);
            entity.Ignore(t => t.JourneyNumber);
            entity.Ignore(t => t.JourneyToCityId);
        }
    }
}