﻿using Services.Api.Query.Services.Data.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Services.Api.Query.Services.Data.Mapping
{
    public class ResourceMap : DbEntityConfiguration<Resource>
    {
        public ResourceMap() : base(tableName: "Resource")
        {
        }

        public override void Configure(EntityTypeBuilder<Resource> entity)
        {
            base.Configure(entity);
        }
    }
}