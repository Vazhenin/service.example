﻿using Services.Api.Query.Services.Data.Entities;
using Services.Api.Query.Services.Data.Entities.Journeys;
using Services.Api.Query.Services.Data.Entities.Services;
using Services.Api.Query.Services.Data.Extensions;
using Services.Api.Query.Services.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Services.Api.Query.Services.Data
{
    public class CabinetDbContext : DbContext
    {
        public CabinetDbContext(DbContextOptions options) : base(options)
        {
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public DbSet<Journey> Journeys { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostReceipt> PostReceipts { get; set; }

        public DbSet<Principal> Principals { get; set; }

        public DbSet<BusinessTrip> BusinessTrips { get; set; }

        public DbSet<BusinessTripService> BusinessTripServices { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<Traveller> Traveller { get; set; }

        public DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //пока так: https://stackoverflow.com/questions/26957519/ef-core-mapping-entitytypeconfiguration
            modelBuilder.AddConfiguration(new JourneyMap());
            modelBuilder.AddConfiguration(new PrincipalMap());
            modelBuilder.AddConfiguration(new DocumentMap());
            modelBuilder.AddConfiguration(new ResourceMap());
            modelBuilder.AddConfiguration(new PostMap());
            modelBuilder.AddConfiguration(new BusinessTripMap());
            modelBuilder.AddConfiguration(new BusinessTripServiceMap());
            modelBuilder.AddConfiguration(new BusinessTripCustomPropertyValueMap());
            modelBuilder.AddConfiguration(new BusinessTripExpensesMap());
            modelBuilder.AddConfiguration(new ServiceMap());
            modelBuilder.AddConfiguration(new AviaServiceMap());
            modelBuilder.AddConfiguration(new AviaLegMap());
            modelBuilder.AddConfiguration(new AviaSegmentMap());
            modelBuilder.AddConfiguration(new HotelServiceMap());
            modelBuilder.AddConfiguration(new HotelMealMap());
            modelBuilder.AddConfiguration(new RailServiceMap());
            modelBuilder.AddConfiguration(new TransferServiceMap());
            modelBuilder.AddConfiguration(new AeroExpressServiceMap());
            modelBuilder.AddConfiguration(new PostReceiptMap());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(new LoggerFactory(new ILoggerProvider[] { new LoggerProvider() }));
        }
    }
}
