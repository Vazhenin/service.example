﻿namespace Services.Api.Query.Services.Data.Entities
{
    /// <summary>
    /// Значение доп данного.
    /// </summary>
    public class CustomPropertyValue : BaseEntity
    {
        /// <summary>
        /// Русское имя.
        /// </summary>
        public string NameRu { get; set; }

        /// <summary>
        /// Латинское имя.
        /// </summary>
        public string NameEn { get; set; }

        /// <summary>
        /// Латинское значение.
        /// </summary>
        public string ValueEn { get; set; }

        /// <summary>
        /// Русское значение.
        /// </summary>
        public string ValueRu { get; set; }

    }
}
