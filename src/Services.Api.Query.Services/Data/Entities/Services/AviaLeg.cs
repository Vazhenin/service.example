﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class AviaLeg : BaseEntity
    {
        public int ServiceId { get; set; }
        public int? DepartureFromAirportId { get; set; }
        public int? ArrivalToAirportId { get; set; }

        public DateTime? DepartureAt { get; set; }

        public DateTime? ArrivalAt { get; set; }

        public long? Duration { get; set; }

        public ICollection<AviaSegment> Segments { get; set; }
    }
}

