﻿using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class AeroExpressService : BaseEntity
    {
        public AeroExpressTariffClass TariffClass { get; set; }
        public long? DepartureAt { get; set; }
        public long? ArrivalAt { get; set; }
        public int DepartureStation { get; set; }
        public int ArrivalStation { get; set; }
    }
}
