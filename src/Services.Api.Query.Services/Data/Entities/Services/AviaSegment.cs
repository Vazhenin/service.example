﻿using System;
using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class AviaSegment : BaseEntity
    {

        public int LegId { get; set; }

        public CabinClass CabinClass { get; set; }

        public int? BaggageQuantity { get; set; }
        public int? BaggageWeightMeasurement { get; set; }
        public int? BaggageType { get; set; }



        public DateTime DepartureAt { get; set; }
        public int? DepartureFromAirportId { get; set; }
        public DateTime ArrivalAt { get; set; }
        public int? ArrivalToAirportId { get; set; }

        public long? Duration { get; set; }

        public string MarketingAirlineCode { get; set; }

        public string FlightNumber { get; set; }

        /// <summary>
        /// Это уже детальней некуда
        /// ClassOfService Y, W, B, E, N, K, M, NL
        /// </summary>
        public string ClassOfService { get; set; }

        /// <summary>
        /// Детализация CabinClass
        /// Code Business - C, First - F, Economy - Y, W, .....
        /// </summary>
        public string CabinClassCode { get; set; }

        public int? StopCount { get; set; }

        public string AircraftCode { get; set; }

        public DateTime? ActualDepartureAt { get; set; }

        public DateTime? ActualArrivalAt { get; set; }

        public FlightStatus FlightStatus { get; set; }

        public string AirCompanyReservationControlNumber { get; set; }

        public string DepartureFromTerminal { get; set; }

        public string ArrivalToTerminal { get; set; }
    }
}
