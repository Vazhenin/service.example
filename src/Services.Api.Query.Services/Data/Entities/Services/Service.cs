﻿using System;
using Services.Api.Query.Model.Enum;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class Service : BaseEntity
    {
        public int Number { get; set; }
        public int? OrderId { get; set; }
        public int ApplicationId { get; set; }
        public ServiceType Type { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? StartOn { get; set; }
        public DateTime? StartOnUtc { get; set; }
        public DateTime? EndOn { get; set; }
        public DateTime? EndOnUtc { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public decimal TariffTotal { get; set; }

        public bool? IsCorporateTariff { get; set; }
        public bool? IsTravelPolicyCompliant { get; set; }
        public string TravelPolicyViolationReason { get; set; }

        public ServiceStatus Status { get; set; }
        public ServicingStatus ServicingStatus { get; set; }
        public ServicingRequest ServicingRequest { get; set; }
        public ReservationAssertion ReservationAssertion { get; set; }
        public ReservationStatus ReservationStatus { get; set; }
        public ExecutionStatus ExecutionStatus { get; set; }
        public ExecutionAssertion ExecutionAssertion { get; set; }
        public RefundStatus? RefundStatus { get; set; }
        public RefundAssertion? RefundAssertion { get; set; }
        public AuthorizationAssertion AuthorizationAssertion { get; set; }
        public AuthorizationStatus AuthorizationStatus { get; set; }

        public Order Order { get; set; }
        public AviaService AviaService { get; set; }
        public HotelService HotelService { get; set; }
        public RailService RailService { get; set; }
        public TransferService TransferService { get; set; }
        public AeroExpressService AeroExpressService { get; set; }

        public byte[] Version { get; set; }
    }
}
