﻿namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class HotelMeal : BaseEntity
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public int ServiceId { get; set; }

        public bool IsIncludedInPrice { get; set; }
    }
}
