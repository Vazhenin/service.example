﻿using System.Collections.Generic;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class AviaService : BaseEntity
    {
        public ICollection<AviaLeg> Legs { get; set; }
    }
}
