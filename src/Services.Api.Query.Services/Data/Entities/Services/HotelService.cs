﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Services.Data.Entities.Services
{
    public class HotelService : BaseEntity
    {
        public int? HotelId { get; set; }
        public int? CityId { get; set; }

        public int? SaleType { get; set; }

        public string RoomNameRu { get; set; }
        public string RoomNameEn { get; set; }

        public DateTime? ActualCheckinAt { get; set; }

        public DateTime? ActualCheckoutAt { get; set; }

        public ICollection<HotelMeal> Meals { get; set; }

        public string ConfirmationToken { get; set; }

        public decimal? EarlyCheckinCost { get; set; }
        public decimal? LateCheckoutCost { get; set; }

        public bool? IsPayOnSite { get; set; }
    }
}
