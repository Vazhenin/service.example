﻿namespace Services.Api.Query.Services.Data.Entities
{
    using System;

    /// <summary>
    /// Прочитанные сособщения.
    /// </summary>
    public class PostReceipt : BaseEntity
    {
        /// <summary>
        /// Идентификатор соощения.
        /// </summary>
        public int PostID { get; set; }

        /// <summary>
        /// Идентификатор прочитавшего сообщение.
        /// </summary>
        public int ReadByPrincipalID { get; set; }

        /// <summary>
        /// Дата и время прочтения.
        /// </summary>
        public DateTime ReadAt { get; set; }
    }
}