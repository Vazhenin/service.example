﻿using System;
using System.Collections.Generic;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data.Entities.Services;

namespace Services.Api.Query.Services.Data.Entities.Journeys
{
    public class BusinessTrip : BaseEntity
    {
        public int CompanyId { get; set; }
        public int ProfileId { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedByPrincipalId { get; set; }
        public int Number { get; set; }
        public int JourneyNumber { get; set; }
        public decimal? Budget { get; set; }

        public AuthorizationAssertion AuthorizationAssertion { get; set; }
        public AuthorizationStatus AuthorizationStatus { get; set; }
        public AuthorizationAssertion CompleteTripAuthorizationAssertion { get; set; }
        public AuthorizationStatus CompleteTripAuthorizationStatus { get; set; }

        public int TravelFromCityId { get; set; }
        public int JourneyToCityId { get; set; }
        public PaymentType? RequisitesPaymentMethod { get; set; }
        public int? RequisitesBankCardId { get; set; }

        public IEnumerable<BusinessTripService> BusinessTripServices { get; set; }
        public IEnumerable<BusinessTripCustomPropertyValue> CustomPropertyValues { get; set; }
        public IEnumerable<BusinessTripExpenses> BusinessTripExpenses { get; set; }
        public IEnumerable<Post> Posts { get; set; }

        public TripStatus Status { get; set; }
        //TODO: тип
        public int? OperationStatus { get; set; }

        public bool IsAnyDocuments { get; set; }
        public int? JourneyId { get; set; }
        public Journey Journey { get; set; }
    }
}
