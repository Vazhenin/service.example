﻿namespace Services.Api.Query.Services.Data.Entities.Journeys
{
    using System;
    using System.Collections.Generic;

    public class Journey : BaseEntity
    {
        public DateTime CreatedAt { get; set; }
        public int CreatedByPrincipalId { get; set; }
        public int Number { get; set; }
        public int JourneyToCityId { get; set; }
        public DateTime StartsOn { get; set; }
        public DateTime EndsOn { get; set; }
        public int CompanyId { get; set; }
        public DateTime? DeletedAt { get; set; }
        public int? DeletedByPrincipalId { get; set; }
        public IList<BusinessTrip> BusinessTrips { get; set; }
        
    }
}
