﻿namespace Services.Api.Query.Services.Data.Entities.Journeys
{
    public class BusinessTripExpenses : BaseEntity
    {

        public decimal Cost { get; set; }

        public string ExpensesSnapshotNameRu { get; set; }

        public string ExpensesSnapshotNameEn { get; set; }

        public int BusinessTripId { get; set; }

        public BusinessTrip BusinessTrip { get; set; }
    }
}
