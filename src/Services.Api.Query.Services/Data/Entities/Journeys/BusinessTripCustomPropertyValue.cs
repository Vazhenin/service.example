﻿namespace Services.Api.Query.Services.Data.Entities.Journeys
{
    public class BusinessTripCustomPropertyValue : CustomPropertyValue
    {
        public int BusinessTripId { get; set; }

        public BusinessTrip BusinessTrip { get; set; }
    }
}
