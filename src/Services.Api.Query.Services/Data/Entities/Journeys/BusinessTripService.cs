﻿using Services.Api.Query.Services.Data.Entities.Services;
namespace Services.Api.Query.Services.Data.Entities.Journeys
{

    public class BusinessTripService : BaseEntity
    {
        public int BusinessTripId { get; set; }
        public int ServiceId { get; set; }
        public bool IsPrimary { get; set; }
        public BusinessTrip BusinessTrip { get; set; }
        public Service Service { get; set; }
    }
}
