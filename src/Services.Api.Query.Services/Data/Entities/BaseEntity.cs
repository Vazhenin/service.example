﻿namespace Services.Api.Query.Services.Data.Entities
{
    /// <summary>
    /// Абстрактный базовый класс для всех сущностей БД.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Главный идентификатор сущности.
        /// </summary>
        public int Id { get; set; }
    }
}