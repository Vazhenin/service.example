﻿namespace Services.Api.Query.Services.Data.Entities
{
    public class TripServicesCount
    {
        public int BusinessTripId { get; set; }

        public int ActiveServicesCount { get; set; }

        public int ServicesCount { get; set; }
    }
}
