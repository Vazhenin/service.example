﻿namespace Services.Api.Query.Services.Data.Entities
{
    public class Resource : BaseEntity
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public int ContentLength { get; set; }

        public string Fingerprint { get; set; }
    }
}
