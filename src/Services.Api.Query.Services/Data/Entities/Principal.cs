﻿namespace Services.Api.Query.Services.Data.Entities
{
    public class Principal : BaseEntity
    {
        public int? EmployeeId { get; set; }
        public int? ProfileId { get; set; }
    }
}