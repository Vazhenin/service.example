﻿using System;
using System.Collections.Generic;

namespace Services.Api.Query.Services.Data.Entities
{

    public class Document : BaseEntity
    {
        public int ServiceId { get; set; }

        public DateTime? DeletedAt { get; set; }

        public string TicketToken { get; set; }

        public ICollection<Resource> Attachments { get; set; }
    }
}
