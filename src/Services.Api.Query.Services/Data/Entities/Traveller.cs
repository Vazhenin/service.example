﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Data.Entities
{
    public class Traveller : BaseEntity
    {
        public int ServiceId { get; set; }

        public int ProfileId { get; set; }
    }
}
