﻿using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data.Entities.Journeys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.Api.Query.Services.Data.Entities
{
    public class Post : BaseEntity
    {
        public int? PostedByPrincipalId { get; set; }
        public DateTime? PostedAt { get; set; }
        public string Content { get; set; }

        public bool IsPrivate { get; set; }

        public ServicingStatus ServicingStatus { get; set; }

        public int Type { get; set; }

        //public int OrderId { get; set; }

        public int? BusinessTripId { get; set; }

        public BusinessTrip BusinessTtip { get; set; }

        public IEnumerable<Resource> Attachments { get; set; }

        public IEnumerable<PostReceipt> Receipts { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
    }
}
