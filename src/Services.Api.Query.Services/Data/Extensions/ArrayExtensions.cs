﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Data.Extensions
{
    static class ArrayExtensions
    {
        public static int Compare(this byte[] b1, byte[] b2)
        {
            // you can as well just throw NotImplementedException here, EF will not call this method directly
            if (b1 == null && b2 == null)
                return 0;
            else if (b1 == null)
                return -1;
            else if (b2 == null)
                return 1;
            return ((IStructuralComparable)b1).CompareTo(b2, Comparer<byte>.Default);
        }
    }
}
