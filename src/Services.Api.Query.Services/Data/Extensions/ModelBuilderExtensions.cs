﻿using Services.Api.Query.Services.Data.Entities;
using Services.Api.Query.Services.Data.Mapping;
using Microsoft.EntityFrameworkCore;

namespace Services.Api.Query.Services.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void AddConfiguration<TEntity>(
            this ModelBuilder modelBuilder,
            DbEntityConfiguration<TEntity> entityConfiguration) where TEntity : BaseEntity
        {
            modelBuilder.Entity<TEntity>(entityConfiguration.Configure);
        }
    }
}