﻿using System.Linq;
using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Services.Data.Extensions.Queries
{
    public static class BusinessTripExtensions
    {
        /// <summary>
        /// Получить командировку
        /// </summary>
        /// <param name="context">Контекст БД</param>
        /// <param name="businessTripId">Идентификатор командировки</param>
        /// <returns></returns>
        public static BusinessTrip GetBusinessTrip(this CabinetDbContext context, int businessTripId)
        {
            var businessTrip = (from t in context.BusinessTrips
                                join pr in context.Principals on t.CreatedByPrincipalId equals pr.Id into prncpls
                                from principals in prncpls.DefaultIfEmpty()
                                where t.Id == businessTripId
                                select new BusinessTrip
                                {
                                    Id = t.Id,
                                    Number = t.Number,
                                    JourneyNumber = t.Journey.Number,
                                    JourneyToCityId = t.Journey.JourneyToCityId,
                                    StartsOn = t.StartsOn,
                                    EndsOn = t.EndsOn,
                                    ProfileId = t.ProfileId,
                                    CompanyId = t.CompanyId,
                                    RequisitesPaymentMethod = t.RequisitesPaymentMethod,
                                    RequisitesBankCardId = t.RequisitesBankCardId,
                                    AuthorizationAssertion = t.AuthorizationAssertion,
                                    TravelFromCityId = t.TravelFromCityId,
                                    AuthorizationStatus = t.AuthorizationStatus,
                                    CompleteTripAuthorizationAssertion = t.CompleteTripAuthorizationAssertion,
                                    CompleteTripAuthorizationStatus = t.CompleteTripAuthorizationStatus,
                                    CreatedAt = t.CreatedAt,
                                    CreatedByProfileId = principals.ProfileId ?? 0,
                                    CustomPropertyValues = t.CustomPropertyValues.Select(v =>
                                        new CustomPropertyValue
                                        {
                                            NameRu = v.NameRu,
                                            ValueEn = v.ValueEn,
                                            NameEn = v.NameEn,
                                            ValueRu = v.ValueRu
                                        }).ToArray(),
                                    BusinessTripExpenses = t.BusinessTripExpenses.Select(s =>
                                        new BusinessTripExpenses
                                        {
                                            Cost = s.Cost,
                                            ExpensesSnapshotNameEn = s.ExpensesSnapshotNameEn,
                                            ExpensesSnapshotNameRu = s.ExpensesSnapshotNameRu
                                        }).ToArray(),
                                    Budget = t.Budget,
                                    HasDocuments = (from bts in t.BusinessTripServices
                                                    join d in context.Documents on bts.ServiceId equals d.ServiceId
                                                    where !d.DeletedAt.HasValue
                                                    select d.Id).Any()
                                }).FirstOrDefault();

            return businessTrip;
        }


        public static BusinessTripInfo GetBusinessTripInfo(this CabinetDbContext context, int businessTripId)
        {
            var businessTrip = (from t in context.BusinessTrips
                                join pr in context.Principals on t.CreatedByPrincipalId equals pr.Id into prncpls
                                from principals in prncpls.DefaultIfEmpty()
                                where t.Id == businessTripId
                                select new BusinessTripInfo
                                {
                                    Id = t.Id,
                                    Number = t.Number,
                                    JourneyNumber = t.Journey.Number,
                                    JourneyToCityId = t.Journey.JourneyToCityId,
                                    StartsOn = t.StartsOn,
                                    EndsOn = t.EndsOn,
                                    ProfileId = t.ProfileId,
                                    CompanyId = t.CompanyId,
                                    AuthorizationAssertion = t.AuthorizationAssertion,
                                    TravelFromCityId = t.TravelFromCityId,
                                    AuthorizationStatus = t.AuthorizationStatus,
                                    CreatedAt = t.CreatedAt,
                                    CreatedByProfileId = (int)principals.ProfileId,
                                    Budget = t.Budget,
                                }).FirstOrDefault();

            return businessTrip;
        }

        public static BusinessTripNumber GetBusinessTripNumberById(this CabinetDbContext context,
           int businessTripId)
        {
            var number = (from t in context.BusinessTrips
                          where t.Id == businessTripId
                          select new BusinessTripNumber
                          {
                              JourneyNumber = t.Journey.Number,
                              TripNumber = t.Number
                          }).FirstOrDefault();

            return number;
        }

        public static BusinessTripNumber GetBusinessTripNumberByServiceId(this CabinetDbContext context,
            int serviceId)
        {
            var number = (from t in context.BusinessTrips
                          where t.BusinessTripServices.Any(ts => ts.ServiceId == serviceId)
                          select new BusinessTripNumber
                          {
                              JourneyNumber = t.Journey.Number,
                              TripNumber = t.Number
                          }).FirstOrDefault();

            return number;
        }


        public static int? GetBusinessTripIdByNumber(this CabinetDbContext context,
           int journeyNumber,
           int businessTripNumber)
        {
            var id = (from t in context.BusinessTrips
                      where t.Number == businessTripNumber && t.Journey.Number == journeyNumber
                      select t.Id).FirstOrDefault();

            if (id == 0) return null;
            return id;
        }


        public static BusinessTripProfileId GetBusinessTripProfileByNumber(this CabinetDbContext context, int journeyNumber, int businessTripNumber)
        {
            var profileId = (from t in context.BusinessTrips
                             where t.Number == businessTripNumber && t.Journey.Number == journeyNumber
                             select new BusinessTripProfileId
                             {
                                 Id = t.Id,
                                 ProfileId = t.ProfileId
                             }).FirstOrDefault();

            return profileId;
        }
    }
}
