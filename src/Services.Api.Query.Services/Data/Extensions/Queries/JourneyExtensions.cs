﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Services.Api.Query.Services.Data.Extensions.Queries
{
    public static class JourneyExtensions
    {
        public static int? GetJourneyNumberById(this CabinetDbContext dbContext, int journeyId)
        {
            var number = (from j in dbContext.Journeys
                          where j.Id == journeyId
                          select j.Number).FirstOrDefault();

            if (number == 0) return null;
            return number;
        }
    }
}
