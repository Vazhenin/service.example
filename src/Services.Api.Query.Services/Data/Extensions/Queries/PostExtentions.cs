﻿using Services.Api.Query.Services.Data.Projections;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Services.Api.Query.Services.Data.Extensions.Queries
{
    public static class PostExtentions
    {
        public static IQueryable<PostProjection> GetPostsReadBy(this DbSet<Entities.Post> dbSet,
            int journeyNumber,
            int businessTripNumber,
            long? version,
            int? lastMessageId,
            int lastCount,
            int readByPrincipalId)
        {
            var posts = dbSet
                .Where(p => p.BusinessTtip.Journey.Number == journeyNumber && p.BusinessTtip.Number == businessTripNumber && !p.IsPrivate);

            if (version != null) posts = posts.Where(p => (long)((object)p.Version) > version);
            if (lastMessageId.HasValue) posts = posts.Where(p => p.Id < lastMessageId.Value);

            var projection = posts
                .OrderByDescending(p => p.PostedAt)
                .Take(lastCount)
                .Select(p => new PostProjection
                {
                    Id = p.Id,
                    Message = p.Content,
                    PostedAt = p.PostedAt,
                    PostedByPrincipalId = p.PostedByPrincipalId,
                    BinaryVersion = p.Version,
                    Attachments = p.Attachments.AsQueryable().Select(ResourceProjections.Projection).ToArray(),
                    IsRead = p.PostedByPrincipalId == readByPrincipalId || p.Receipts.Any(r => r.ReadByPrincipalID == readByPrincipalId),
                    IsPersonMessage = p.PostedByPrincipalId == readByPrincipalId
                });

            return projection;
        }

        public static PostInfoProjection GetPostInfoById(this DbSet<Entities.Post> dbSet, int tripId, int postId)
        {
            var projection = (from p in dbSet
                              where p.Id == postId && p.BusinessTripId == tripId
                              select new PostInfoProjection
                              {
                                  Id = p.Id,
                                  Message = p.Content,
                                  PostedAt = p.PostedAt,
                                  PostedByPrincipalId = p.PostedByPrincipalId
                              }).SingleOrDefault();

            return projection;
        }

    }
}
