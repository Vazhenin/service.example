﻿using System.Linq;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Services.Interfaces;

namespace Services.Api.Query.Services.Data.Extensions.Queries
{
    public static class TravellerExtensions
    {
        public static int? GetTravellerIdByServiceIdProfileId(this CabinetDbContext context,
            int serviceId,
            int profileId)
        {
            var travellerId = (from t in context.Traveller
                               where t.ServiceId == serviceId && t.ProfileId == profileId
                               select t.Id).FirstOrDefault();

            if (travellerId == 0) return null;

            return travellerId;
        }
    }
}
