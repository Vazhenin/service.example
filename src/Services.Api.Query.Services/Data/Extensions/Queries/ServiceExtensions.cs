﻿using System.Collections.Generic;
using System.Linq;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data.Entities.Services;
using Microsoft.EntityFrameworkCore;
using System;
using Services.Api.Query.Model.Search;

namespace Services.Api.Query.Services.Data.Extensions.Queries
{
    public static class ServiceExtensions
    {
        public static IList<ServiceCard> GetTripServices(this CabinetDbContext context, int businessTripId)
        {
            var query = GetServicesQuery(context);

            var dbServices = (from s in query
                              join bts in context.BusinessTripServices on s.Id equals bts.ServiceId
                              where bts.BusinessTripId == businessTripId
                              select s).ToList();

            var services = new List<ServiceCard>();

            foreach (var dbService in dbServices)
            {
                var service = ConvertToServiceCard(dbService);

                if (service != null)
                    services.Add(service);
            }

            return services;
        }

        public static IList<ServiceCard> GetTripServicesForAuthorization(this CabinetDbContext context, int businessTripId)
        {
            var servicesQuery = GetServicesQuery(context);

            var dbServices = from t in context.BusinessTrips
                             where t.Id == businessTripId
                             from bts in t.BusinessTripServices
                             from s in servicesQuery
                             where s.Id == bts.ServiceId
                             select s;

            var tripDbServices = dbServices.Where(s =>
                s.AuthorizationAssertion == AuthorizationAssertion.Required &&
                s.AuthorizationStatus == AuthorizationStatus.InProgress).ToList();

            var services = new List<ServiceCard>();

            foreach (var dbService in tripDbServices)
            {
                var service = ConvertToServiceCard(dbService);

                if (service != null)
                    services.Add(service);
            }

            return services;
        }

        public static IQueryable<Service> GetServicesQuery(this CabinetDbContext context)
        {
            return
                from s in context.Services
                    .Include(l => l.Order)
                    .Include(l => l.AviaService)
                    .ThenInclude(l => l.Legs)
                    .ThenInclude(l => l.Segments)
                    .Include(l => l.HotelService)
                    .ThenInclude(l => l.Meals)
                    .Include(l => l.RailService)
                    .Include(l => l.TransferService)
                    .Include(l => l.AeroExpressService)
                orderby s.UpdatedAt descending
                select s;
        }

        public static ServiceCard ConvertToServiceLite(this Service dbService)
        {
            switch (dbService?.Type)
            {
                case ServiceType.Avia:
                    return GetAviaServiceLite(dbService);
                case ServiceType.Rail:
                    return GetRailServiceCard(dbService);
                case ServiceType.Hotel:
                    return GetHotelServiceLite(dbService);
                case ServiceType.AeroExpress:
                    return GetAeroExpressServiceCard(dbService);
                case ServiceType.Transfer:
                    return GetTransferServiceCard(dbService);
                default:
                    return null;
            }
        }

        private static ServiceCard ConvertToServiceCard(Service dbService)
        {
            switch (dbService?.Type)
            {
                case ServiceType.Avia:
                    return GetAviaServiceCard(dbService);
                case ServiceType.Rail:
                    return GetRailServiceCard(dbService);
                case ServiceType.Hotel:
                    return GetHotelServiceCard(dbService);
                case ServiceType.AeroExpress:
                    return GetAeroExpressServiceCard(dbService);
                case ServiceType.Transfer:
                    return GetTransferServiceCard(dbService);
                default:
                    return null;
            }
        }
        
        private static T GetService<T>(Service dbService) where T : ServiceCard, new()
        {
            return new T
            {
                Id = dbService.Id,
                Number = dbService.Number,
                ApplicationId = dbService.ApplicationId,
                OrderNumber = (dbService.Order?.Number).GetValueOrDefault(),
                Type = dbService.Type,
                StartOn = dbService.StartOn,
                StartOnUtc = dbService.StartOnUtc,
                EndOn = dbService.EndOn,
                EndOnUtc = dbService.EndOnUtc,
                TariffTotal = dbService.TariffTotal,
                IsCorporateTariff = dbService.IsCorporateTariff,
                IsTravelPolicyCompliant = dbService.IsTravelPolicyCompliant,
                TravelPolicyViolationReason = dbService.TravelPolicyViolationReason,
                Status = dbService.Status,
                ServicingRequest = dbService.ServicingRequest,
                ServicingStatus = dbService.ServicingStatus,
                AuthorizationAssertion = dbService.AuthorizationAssertion,
                AuthorizationStatus = dbService.AuthorizationStatus,
                ReservationAssertion = dbService.ReservationAssertion,
                ReservationStatus = dbService.ReservationStatus,
                ExecutionAssertion = dbService.ExecutionAssertion,
                ExecutionStatus = dbService.ExecutionStatus,
                RefundAssertion = dbService.RefundAssertion,
                RefundStatus = dbService.RefundStatus,
                Version = dbService.Version != null ? BitConverter.ToUInt64(dbService.Version.Reverse().ToArray(), 0) : 0
            };
        }

        #region Service card

        private static AviaServiceCard GetAviaServiceCard(Service dbService)
        {
            var dbAvia = dbService.AviaService;

            var aviaService = GetService<AviaServiceCard>(dbService);

            var legs = dbAvia.Legs.Select(l => new AviaLegCard
            {
                DepartureFromAirportId = l.DepartureFromAirportId,
                ArrivalToAirportId = l.ArrivalToAirportId,
                ArrivalAt = l.ArrivalAt,
                DepartureAt = l.DepartureAt,
                Duration = l.Duration,
                Transfers = l.Segments.Count - 1,
                Segments = l.Segments.Select(s => new AviaSegmentCard
                {
                    CabinClass = s.CabinClass,
                    BaggageQuantity = s.BaggageQuantity,
                    BaggageType = s.BaggageType,
                    BaggageWeightMeasurement = s.BaggageWeightMeasurement,
                    MarketingAirlineCode = s.MarketingAirlineCode
                }).ToArray()
            }).ToArray();

            aviaService.Legs = legs;

            return aviaService;
        }

        private static RailServiceCard GetRailServiceCard(Service dbService)
        {
            var dbRail = dbService.RailService;

            var railService = GetService<RailServiceCard>(dbService);

            railService.TrainCode = dbRail.TrainCode;
            railService.TrainName = dbRail.TrainName;
            railService.DirectionGroup = dbRail.DirectionGroup;
            railService.PublicTrainCode = dbRail.PublicTrainCode;
            railService.TrainCategory = dbRail.TrainCategory;
            railService.CarriageNumber = dbRail.CarriageNumber;
            railService.CarriageType = dbRail.CarriageType;
            railService.DepartureFromStationId = dbRail.DepartureFromStationId;
            railService.ArrivalToStationId = dbRail.ArrivalToStationId;
            railService.IsElectronicRegistrationAllowed = dbRail.IsElectronicRegistrationAllowed;
            railService.Duration = dbRail.Duration;
            railService.CarriageClass = dbRail.Class;
            railService.CarriageOwner = dbRail.CarriageOwner;


            return railService;
        }

        private static HotelServiceCard GetHotelServiceCard(Service dbService)
        {
            var dbHotel = dbService.HotelService;

            var hotelService = GetService<HotelServiceCard>(dbService);

            hotelService.HotelId = dbHotel.HotelId;
            hotelService.CityId = dbHotel.CityId;
            hotelService.ActualCheckinAt = dbHotel.ActualCheckinAt;
            hotelService.RoomNameRu = dbHotel.RoomNameRu;
            hotelService.SaleType = dbHotel.SaleType;
            hotelService.ActualCheckoutAt = dbHotel.ActualCheckoutAt;
            hotelService.Meals = dbHotel.Meals.Select(m => new HotelMealCard
            {
                Code = m.Code,
                Name = m.Name
            }).ToArray();

            return hotelService;
        }

        private static AeroExpressServiceCard GetAeroExpressServiceCard(Service dbService)
        {
            var dbAeroExpress = dbService.AeroExpressService;

            var aeroExpressService = GetService<AeroExpressServiceCard>(dbService);

            aeroExpressService.TariffClass = dbAeroExpress.TariffClass;
            aeroExpressService.DepartureAt = TimeSpan.FromTicks(dbAeroExpress.DepartureAt ?? 0);
            aeroExpressService.ArrivalAt = TimeSpan.FromTicks(dbAeroExpress.ArrivalAt ?? 0);
            aeroExpressService.DepartureStation = dbAeroExpress.DepartureStation;
            aeroExpressService.ArrivalStation = dbAeroExpress.ArrivalStation;

            return aeroExpressService;
        }

        private static TransferServiceCard GetTransferServiceCard(Service dbService)
        {
            var dbTransfer = dbService.TransferService;

            var transferService = GetService<TransferServiceCard>(dbService);

            transferService.CarClassification = dbTransfer.CarClassification;
            transferService.ArrivalToAddressEn = dbTransfer.ArrivalToAddressEn;
            transferService.ArrivalToAddressRu = dbTransfer.ArrivalToAddressRu;
            transferService.ArrivalToAirportId = dbTransfer.ArrivalToAirportId;
            transferService.ArrivalToCityId = dbTransfer.ArrivalToCityId;
            transferService.ArrivalToLocation = dbTransfer.ArrivalToLocation;
            transferService.ArrivalToRailwayStationId = dbTransfer.ArrivalToRailwayStationId;
            transferService.ArrivalToType = dbTransfer.ArrivalToType;
            transferService.DepartureFromAddressEn = dbTransfer.DepartureFromAddressEn;
            transferService.DepartureFromAddressRu = dbTransfer.DepartureFromAddressRu;
            transferService.DepartureFromAirportId = dbTransfer.DepartureFromAirportId;
            transferService.DepartureFromCityId = dbTransfer.DepartureFromCityId;
            transferService.DepartureFromLocation = dbTransfer.DepartureFromLocation;
            transferService.DepartureFromRailwayStationId = dbTransfer.DepartureFromRailwayStationId;
            transferService.DepartureFromType = dbTransfer.DepartureFromType;
            transferService.FlightNumber = dbTransfer.FlightNumber;
            transferService.PickupCardText = dbTransfer.PickupCardText;

            return transferService;
        }

        #endregion Service card

        #region Service lite

        private static AviaServiceLite GetAviaServiceLite(Service dbService)
        {
            var dbAvia = dbService.AviaService;

            var aviaService = GetService<AviaServiceLite>(dbService);

            var legs = dbAvia.Legs.Select(l => new AviaLegLite
            {
                Id = l.Id,
                DepartureFromAirportId = l.DepartureFromAirportId,
                ArrivalToAirportId = l.ArrivalToAirportId,
                ArrivalAt = l.ArrivalAt,
                DepartureAt = l.DepartureAt,
                Duration = l.Duration,
                Segments = l.Segments.Select(s => new AviaSegmentLite
                {
                    Id = s.Id,
                    CabinClass = s.CabinClass,
                    BaggageQuantity = s.BaggageQuantity,
                    BaggageType = s.BaggageType,
                    BaggageWeightMeasurement = s.BaggageWeightMeasurement,
                    Duration = s.Duration,
                    ArrivalToAirportId = s.ArrivalToAirportId,
                    DepartureFromAirportId = s.DepartureFromAirportId,
                    DepartureAt = s.DepartureAt,
                    ArrivalAt = s.ArrivalAt,
                    MarketingAirlineCode = s.MarketingAirlineCode,
                    ActualArrivalAt = s.ActualArrivalAt,
                    ActualDepartureAt = s.ActualDepartureAt,
                    AirCompanyReservationControlNumber = s.AirCompanyReservationControlNumber,
                    AircraftCode = s.AircraftCode,
                    CabinClassCode = s.CabinClassCode,
                    ClassOfService = s.ClassOfService,
                    FlightNumber = s.FlightNumber,
                    FlightStatus = s.FlightStatus,
                    StopCount = s.StopCount,
                    DepartureFromTerminal = s.DepartureFromTerminal,
                    ArrivalToTerminal = s.ArrivalToTerminal
                }).ToArray()
            }).ToArray();

            aviaService.Legs = legs;

            return aviaService;
        }

        private static HotelServiceLite GetHotelServiceLite(Service dbService)
        {
            var dbHotel = dbService.HotelService;

            var hotelService = GetService<HotelServiceLite>(dbService);

            hotelService.Id = dbHotel.Id;
            hotelService.HotelId = dbHotel.HotelId;
            hotelService.CityId = dbHotel.CityId;
            hotelService.ActualCheckinAt = dbHotel.ActualCheckinAt;
            hotelService.RoomNameRu = dbHotel.RoomNameRu;
            hotelService.RoomNameEn = dbHotel.RoomNameEn;
            hotelService.SaleType = dbHotel.SaleType;
            hotelService.ActualCheckoutAt = dbHotel.ActualCheckoutAt;
            hotelService.ConfirmationToken = dbHotel.ConfirmationToken;
            hotelService.EarlyCheckinCost = dbHotel.EarlyCheckinCost;
            hotelService.LateCheckoutCost = dbHotel.LateCheckoutCost;
            hotelService.IsPayOnSite = dbHotel.IsPayOnSite;

            hotelService.Meals = dbHotel.Meals.Select(m => new HotelMealCard
            {
                Code = m.Code,
                Name = m.Name,
                IsIncludedInPrice = m.IsIncludedInPrice
            }).ToArray();

            return hotelService;
        }

        #endregion Service lite
    }
}
