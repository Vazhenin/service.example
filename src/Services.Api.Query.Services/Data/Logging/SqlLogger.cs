﻿using System;
using Microsoft.Extensions.Logging;
using NLog;
using Logging = Microsoft.Extensions.Logging;

namespace Services.Api.Query.Services
{
    public class SqlLogger : Logging.ILogger
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
         
        public void Log<TState>(Logging.LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            _logger.Info(formatter.Invoke(state, exception));
        }

        public bool IsEnabled(Logging.LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}
