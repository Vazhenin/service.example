﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Api.Query.Services.Data.Projections
{
    public class PostProjection : Model.Dto.Post
    {
        public byte[] BinaryVersion { get; set; }

        public int? PostedByPrincipalId { get; set; }
    }


    public class PostInfoProjection : Model.Dto.PostInfo
    {
        public int? PostedByPrincipalId { get; set; }
    }
}
