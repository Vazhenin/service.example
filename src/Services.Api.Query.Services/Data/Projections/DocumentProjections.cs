﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Api.Query.Services.Data.Projections
{
    public class DocumentProjections
    {
        public static Expression<Func<Entities.Document, Model.Dto.Document>> Projection
        {
            get
            {
                return d => new Model.Dto.Document
                {
                    TicketToken = d.TicketToken,
                    Attachments = (from a in d.Attachments.AsQueryable()
                                   select new Model.Dto.Resource
                                   {
                                       ContentLength = a.ContentLength,
                                       ContentType = a.ContentType,
                                       Fingerprint = a.Fingerprint,
                                       Name = a.Name
                                   }).ToArray()
                };
            }
        }
    }
}
