﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Api.Query.Services.Data.Projections
{
    public class ResourceProjections 
    {


        public static Expression<Func<Entities.Resource, Model.Dto.Resource>> Projection
        {
            get
            {
                return r => new Model.Dto.Resource
                {
                    ContentLength = r.ContentLength,
                    ContentType = r.ContentType,
                    Fingerprint = r.Fingerprint,
                    Name = r.Name
                };
            }
        }
    }

}
