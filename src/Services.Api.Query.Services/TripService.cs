﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Data.Entities.Services;
using Services.Api.Query.Services.Data.Extensions;
using Services.Api.Query.Services.Data.Extensions.Queries;
using Services.Api.Query.Services.Data.Projections;
using Services.Api.Query.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.Services
{
    public class TripService : ITripService
    {
        private readonly CabinetDbContext _cabinetDbContext;
        private readonly IMapper _mapper;
        private readonly IPrincipalResolver _principalResolver;
        public TripService(CabinetDbContext cabinetDbContext, IMapper mapper, IPrincipalResolver principalResolver)
        {
            _cabinetDbContext = cabinetDbContext;
            _mapper = mapper;
            _principalResolver = principalResolver;
        }

        public ICollection<Post> GetTripPosts(int journeyNumber, int businessTripNumber, PostParameters postParameters)
        {
            var version = postParameters.VersionToken;
            var lastMessageId = postParameters.LastMessageId;
            var lastCount = postParameters?.Last > 0 ? Math.Min(postParameters.Last.Value, 10) : 10;

            var readByPrincipalId = 0;
            if (postParameters.ReadByProfileId.HasValue)
                readByPrincipalId = _principalResolver.GetPrincipalByProfileId(postParameters.ReadByProfileId.Value)?.Id ?? readByPrincipalId;

            var projection = _cabinetDbContext.Posts.GetPostsReadBy(journeyNumber, businessTripNumber, postParameters?.VersionToken, lastMessageId, lastCount, readByPrincipalId).ToList();

            var postsDto = _mapper.Map<ICollection<Post>>(projection);

            return postsDto;
        }

        public PostInfo GetTripPost(int tripId, int postId)
        {
            var projection = _cabinetDbContext.Posts.GetPostInfoById(tripId, postId);

            var postInfoDto = _mapper.Map<PostInfo>(projection);

            return postInfoDto;
        }

        public ICollection<Document> GetTravellerDocumentsForTrip(int journeyNumber, int tripNumber)
        {
            var documents = (from t in _cabinetDbContext.BusinessTrips.AsNoTracking()
                             from ts in t.BusinessTripServices
                             join d in _cabinetDbContext.Documents on ts.ServiceId equals d.ServiceId
                             where t.Number == tripNumber && t.Journey.Number == journeyNumber && !d.DeletedAt.HasValue
                             select d).Select(DocumentProjections.Projection).ToList();

            return documents;
        }


        public BusinessTrip GetTripByNumber(int journeyNumber, int tripNumber)
        {
            var result = default(BusinessTrip);

            var tripId = _cabinetDbContext.GetBusinessTripIdByNumber(journeyNumber, tripNumber);

            if (tripId.HasValue)
            {
                result = _cabinetDbContext.GetBusinessTrip(tripId.Value);

                if (result != null)
                {
                    var services = _cabinetDbContext.GetTripServices(tripId.Value);

                    result.Services = services.ToArray();
                }
            }

            return result;
        }

        /// <summary>
        /// Получение признака наличия непрочитанных сообщений.
        /// </summary>
        /// <param name="tripId">Идентиифкатор командировки.</param>
        /// <param name="profileId">Идентификатор профиля.</param>
        /// <returns>True если пост прочитан</returns>
        public bool GetPostUnReadStatusByBusinessTripeIdsAndProfile(int tripId, int profileId)
        {
            var principal = _principalResolver.GetPrincipalByProfileId(profileId);
            if (principal != null && principal.Id > 0)
            {
                return (from p in _cabinetDbContext.Posts
                        join pr in _cabinetDbContext.PostReceipts
                            .Where(pr => pr.ReadByPrincipalID == principal.Id) on p.Id equals pr.PostID into rds
                        from reads in rds.DefaultIfEmpty()
                        where (p.PostedByPrincipalId != principal.Id) && (p.BusinessTripId == tripId) && (reads == null)
                        select true).FirstOrDefault();
            }
            else
            {
                throw new InvalidOperationException($"Принципал не найден для профиля: {profileId}");
            }
        }

        public BusinessTripInfo GetTripForAuthorization(int tripId)
        {
            var result = _cabinetDbContext.GetBusinessTripInfo(tripId);

            if (result != null)
            {
                var services = _cabinetDbContext.GetTripServicesForAuthorization(tripId);

                result.Services = services.ToArray();
            }

            return result;
        }

        public BusinessTrip GetTripForAuthorizationDetail(int tripId)
        {
            var businessTrip = _cabinetDbContext.GetBusinessTrip(tripId);

            var result = _mapper.Map<BusinessTrip>(businessTrip);

            if (result != null)
            {
                var services = _cabinetDbContext.GetTripServicesForAuthorization(tripId);

                result.Services = services.ToArray();
            }

            return result;
        }
    }
}
