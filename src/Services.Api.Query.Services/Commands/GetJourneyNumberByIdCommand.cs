﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Data.Extensions.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Commands
{
    /// <summary>
    /// Команда для получения информации
    /// </summary>
    public class GetJourneyNumberByIdCommand : IQueryCommand<JourneyIdParameter, int?>
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public GetJourneyNumberByIdCommand(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public int? Execute(JourneyIdParameter query)
        {
            var number = _cabinetDbContext.GetJourneyNumberById(query.JourneyId);
            return number;
        }
    }
}
