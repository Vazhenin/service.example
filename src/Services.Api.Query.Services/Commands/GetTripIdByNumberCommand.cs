﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Data.Extensions.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Commands
{
    /// <summary>
    /// Команда для получения информации
    /// </summary>
    public class GetTripIdByNumberCommand : IQueryCommand<BusinessTripNumberParameter, int?>
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public GetTripIdByNumberCommand(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public BusinessTripNumber Execute(BusinessTripIdParameter query)
        {
            var number = _cabinetDbContext.GetBusinessTripNumberById(query.TripId);
            return number;
        }

        public int? Execute(BusinessTripNumberParameter query)
        {
            var id = _cabinetDbContext.GetBusinessTripIdByNumber(query.JourneyNumber, query.TripNumber);
            return id;
        }
    }
}
