﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Data.Extensions.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Commands
{
    /// <summary>
    /// Команда для получения информации
    /// </summary>
    public class GetTripProfileIdByTripNumberCommand : IQueryCommand<BusinessTripNumberParameter, BusinessTripProfileId>
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public GetTripProfileIdByTripNumberCommand(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public BusinessTripProfileId Execute(BusinessTripNumberParameter query)
        {
            var profileId = _cabinetDbContext.GetBusinessTripProfileByNumber(query.JourneyNumber, query.TripNumber);
            return profileId;
        }
    }
}
