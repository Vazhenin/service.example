﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Data.Extensions.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Commands
{
    /// <summary>
    /// Команда для получения информации
    /// </summary>
    public class GetTripNumberCommand : IQueryCommand<BusinessTripIdParameter, BusinessTripNumber>, IQueryCommand<BusinessTripServiceIdParameter, BusinessTripNumber>
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public GetTripNumberCommand(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public BusinessTripNumber Execute(BusinessTripIdParameter query)
        {
            var number = _cabinetDbContext.GetBusinessTripNumberById(query.TripId);
            return number;
        }

        public BusinessTripNumber Execute(BusinessTripServiceIdParameter query)
        {
            var number = _cabinetDbContext.GetBusinessTripNumberByServiceId(query.ServiceId);
            return number;
        }
    }
}
