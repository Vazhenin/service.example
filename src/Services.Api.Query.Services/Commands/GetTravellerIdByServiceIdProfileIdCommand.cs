﻿using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Parameters;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Services.Data.Extensions.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Commands
{
    /// <summary>
    /// Команда для получения Id пассажира
    /// </summary>
    public class GetTravellerIdByServiceIdProfileIdCommand : IQueryCommand<ServiceIdProfileIdParameter, int?>
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public GetTravellerIdByServiceIdProfileIdCommand(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public int? Execute(ServiceIdProfileIdParameter query)
        {
            var id = _cabinetDbContext.GetTravellerIdByServiceIdProfileId(query.ServiceId, query.ProfileId);
            return id;
        }
    }
}
