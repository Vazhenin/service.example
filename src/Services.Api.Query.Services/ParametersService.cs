﻿using Services.Api.Query.Model.Dto;
using System;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Services.Api.Query.Services
{
    public class ParametersService : IParametersService
    {
        private readonly CabinetDbContext _cabinetDbContext;

        public ParametersService(CabinetDbContext cabinetDbContext)
        {
            _cabinetDbContext = cabinetDbContext;
        }

        public int GetJourneyNumber(int journeyId)
        {
            return _cabinetDbContext.Journeys.Where(j => j.Id == journeyId).Select(j => j.Number).FirstOrDefault();
        }


        public BusinessTripNumber GetTripNumbers(int tripId)
        {
            return (from bt in _cabinetDbContext.BusinessTrips
                    join j in _cabinetDbContext.Journeys on bt.JourneyId equals j.Id
                    where bt.Id == tripId
                    select new BusinessTripNumber { JourneyNumber = j.Number, TripNumber = bt.Number })
                .FirstOrDefault();
        }
        public object GetQueryParameters(string queryName, IEnumerable<KeyValuePair<string, string>> parameters)
        {
            throw new NotImplementedException();
        }
    }
}
