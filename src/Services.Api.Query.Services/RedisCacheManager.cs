﻿using System;
using System.Net;
using Services.Api.Query.Services.Interfaces;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Services.Api.Query.Services
{
    public class RedisCacheManager : IRedisCacheManager, IDisposable
    {
        private readonly JsonSerializerSettings _settings;

        private readonly string _cacheConnectionString;

        private ConnectionMultiplexer _redis;

        //private readonly Logger<> _log = LogManager.GetCurrentClassLogger();

        public RedisCacheManager(string cacheConnectionString)
        {
            _cacheConnectionString = cacheConnectionString;

            Connect();

            if (_redis != null)
            {
                _redis.ConnectionFailed += RedisOnConnectionFailed;
                _redis.ConnectionRestored += RedisOnConnectionRestored;
            }
            else
            {
                //_log.Warn("_redis isn't initialized");
            }

            _settings = new JsonSerializerSettings
            {
                //TODO: из-за разных неймспейсов не получится держать кеш в одной базе с ордерсом
                //TypeNameHandling = TypeNameHandling.All,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }

        private void Connect()
        {
            try
            {
                _redis = ConnectionMultiplexer.Connect(_cacheConnectionString);

                //_log.Info($"Connected to {_cacheConnectionString}");

                CheckConnections();
            }
            catch (Exception ex)
            {
                //_log.Warn(ex, $"can't connect to redis at {_cacheConnectionString}");
            }
        }

        private void RedisOnConnectionRestored(object sender, ConnectionFailedEventArgs connectionFailedEventArgs)
        {
            //_log.Info($"Connection restored to {connectionFailedEventArgs.EndPoint}");

            CheckConnections();
        }

        private void RedisOnConnectionFailed(object sender, ConnectionFailedEventArgs connectionFailedEventArgs)
        {
            //_log.Info($"Connection failed to {connectionFailedEventArgs.EndPoint}");

            CheckConnections();
        }

        private void CheckConnections()
        {
            try
            {
                var endPoints = _redis.GetEndPoints();

                foreach (var endPoint in endPoints)
                {
                    var dnsEndPoint = (DnsEndPoint)endPoint;
                    var server = _redis.GetServer(dnsEndPoint);

                    //_log.Info($"server {dnsEndPoint.Host}:{dnsEndPoint.Port} is Connected? - {server.IsConnected}");
                    //_log.Info($"server {dnsEndPoint.Host}:{dnsEndPoint.Port} is Slave? - {server.IsSlave}");
                }
            }
            catch (Exception e)
            {
                //_log.Warn(e, $"can't check connections to {_cacheConnectionString}");
            }
        }

        private string SerializeJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, _settings);
        }

        private T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _settings);
        }

        public bool TryGetValue<T>(string key, out T value, int dbIndex)
        {
            try
            {
                var db = _redis.GetDatabase(dbIndex);

                if (!Contains(key, dbIndex))
                {
                    value = default(T);
                    return false;
                }

                var json = (string)db.StringGet(key);

                if (string.IsNullOrEmpty(json))
                {
                    throw new Exception("json from db is empty");
                }

                value = DeserializeJson<T>(json);

                return true;
            }
            catch (Exception e)
            {
                //_log.Warn(e, $"can't get value for key {key}");

                value = default(T);

                return false;
            }
        }

        public void AddOrUpdateValue<T>(string key, T value, TimeSpan lifetime, int dbIndex)
        {
            try
            {
                var db = _redis.GetDatabase(dbIndex);

                var json = SerializeJson(value);

                db.StringSet(key, json, lifetime);
            }
            catch (Exception e)
            {
                //_log.Warn(e, $"can't add or update value for {key}");
            }
        }

        public void Remove(string key, int dbIndex)
        {
            try
            {
                var db = _redis.GetDatabase();
                db.KeyDelete(key);
            }
            catch (Exception e)
            {
                //_log.Warn(e, $"can't remove key {key}");
            }
        }

        public bool Contains(string key, int dbIndex)
        {
            try
            {
                var db = _redis.GetDatabase(dbIndex);
                return db.KeyExists(key);
            }
            catch (Exception e)
            {
                //_log.Warn(e, $"can't check contains of key {key}");

                return false;
            }
        }

        public void Dispose()
        {
            _redis?.Dispose();
        }
    }
}