﻿using System;
using Services.Api.Query.Model;
using Services.Api.Query.Services.Interfaces;
using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Services
{
    public class PrincipalCache : IPrincipalCache
    {
        private readonly IRedisCacheManager _cacheManager;
        private readonly int _databaseIndex;
        private static TimeSpan Lifetime = TimeSpan.FromHours(1);

        public PrincipalCache(IRedisCacheManager cacheManager, int databaseIndex)
        {
            _cacheManager = cacheManager;
            _databaseIndex = databaseIndex;
        }

        private const string PrincipalByIdKey = "principal:id:";
        private const string PrincipalByEmployeeIdKey = "principal:employeeid:";
        private const string PrincipalByProfileIdKey = "principal:profileid:";

        public Principal GetPrincipalByEmployeeId(int employeeId)
        {
            int principalId;
            if (!_cacheManager.TryGetValue(GetEmployeeKey(employeeId), out principalId, _databaseIndex))
                return default(Principal);
            return GetPrincipalById(principalId);
        }

        public Principal GetPrincipalByProfileId(int profileId)
        {
            int principalId;
            if (!_cacheManager.TryGetValue(GetProfileKey(profileId), out principalId, _databaseIndex))
                return default(Principal);
            return GetPrincipalById(principalId);
        }

        public Principal GetPrincipalById(int principalId)
        {
            Principal result;
            _cacheManager.TryGetValue(GetPrincipalKey(principalId), out result, _databaseIndex);
            return result;
        }

        public void AddPrincipal(Principal principal)
        {
            if (principal.EmployeeId.HasValue)
            {
                _cacheManager.AddOrUpdateValue(GetEmployeeKey(principal.EmployeeId.Value), principal.Id, Lifetime, _databaseIndex);
            }
            if (principal.ProfileId.HasValue)
            {
                _cacheManager.AddOrUpdateValue(GetProfileKey(principal.ProfileId.Value), principal.Id, Lifetime, _databaseIndex);
            }
            _cacheManager.AddOrUpdateValue(GetPrincipalKey(principal.Id), principal, Lifetime, _databaseIndex);
        }

        public static string GetPrincipalKey(int principalId)
        {
            return $"{PrincipalByIdKey}{principalId}";
        }
        public static string GetProfileKey(int profileId)
        {
            return $"{PrincipalByProfileIdKey}{profileId}";
        }
        public static string GetEmployeeKey(int employeeId)
        {
            return $"{PrincipalByEmployeeIdKey}{employeeId}";
        }
    }
}