﻿using AutoMapper;
using Services.Api.Query.Services.Data.Projections;
using Services.Api.Query.Services.Mapping.Rules;
using System;
using System.Linq;
namespace Services.Api.Query.Services.Mapping
{

    /// <summary>
    /// Профиль для маппинга.
    /// </summary>
    public class ProjectionProfile : Profile
    {
        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public ProjectionProfile()
        {
            CreateMap<PostProjection, Model.Dto.Post>()
                 .ForMember(d => d.Version, m => m.MapFrom(s => BitConverter.ToUInt64(s.BinaryVersion.Reverse().ToArray(), 0)))
                 .ForMember(d => d.PostedByProfileId, m => m.ResolveUsing<PrincipalToProfileResolver, int?>(s => s.PostedByPrincipalId));

            CreateMap<PostInfoProjection, Model.Dto.PostInfo>()
                 .ForMember(d => d.PostedByProfileId, m => m.ResolveUsing<PrincipalToProfileResolver, int?>(s => s.PostedByPrincipalId));
        }
    }
}