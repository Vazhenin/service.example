﻿using AutoMapper;
using Services.Api.Query.Services.Mapping.Rules;
using JourneyParameters = Services.Api.Query.Model.Dto.Search.JourneyParameters;

namespace Services.Api.Query.Services.Mapping
{

    /// <summary>
    /// Профиль для маппинга.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Data.Entities.Journeys.Journey, Model.Dto.Journey>()
                 .ForMember(d => d.JourneyToCityId, m => m.MapFrom(s => s.JourneyToCityId))
                 .ForMember(d => d.CreatedByProfileId, m => m.ResolveUsing<PrincipalToProfileResolver, int>(s => s.CreatedByPrincipalId));

            CreateMap<Data.Entities.Journeys.BusinessTrip, Model.Dto.BusinessTripInfo>()
                .Include<Data.Entities.Journeys.BusinessTrip, Model.Dto.BusinessTrip>()
                 .ForMember(d => d.CreatedByProfileId, m => m.ResolveUsing<PrincipalToProfileResolver, int>(s => s.CreatedByPrincipalId))
                .ForMember(d => d.Services, m => m.Ignore())
                .ForMember(d => d.ActiveServiceCount, m => m.Ignore())
                .ForMember(d => d.ServiceCount, m => m.Ignore());

            CreateMap<Data.Entities.Journeys.BusinessTrip, Model.Dto.BusinessTrip>()
                .ForMember(d => d.HasDocuments, m => m.Ignore());

            CreateMap<Data.Entities.Journeys.BusinessTripCustomPropertyValue, Model.Dto.CustomPropertyValue>();

            CreateMap<JourneyParameters, Model.Search.JourneyParameters>()
                .ForMember(d => d.CompanyIds, m => m.ResolveUsing<StringToArrayConverter, string>(s => s.CompanyIds))
                .ForMember(d => d.ProfileIds, m => m.ResolveUsing<StringToArrayConverter, string>(s => s.ProfileIds))
                .ForMember(d => d.TravellerProfileIds, m => m.ResolveUsing<StringToArrayConverter, string>(s => s.TravellerProfileIds));
        }
    }
}