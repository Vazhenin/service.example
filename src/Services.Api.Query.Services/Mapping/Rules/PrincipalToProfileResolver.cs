﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Services.Interfaces;

namespace Services.Api.Query.Services.Mapping.Rules
{
    public class PrincipalToProfileResolver :
        IMemberValueResolver<object, object, int, int>,
        IMemberValueResolver<object, object, int?, int?>
    {
        private readonly IPrincipalResolver _principalResolver;


        /// <summary>
        /// Конструктор который нужен для тестов.
        /// </summary>
        public PrincipalToProfileResolver()
        {
        }

        public PrincipalToProfileResolver(IPrincipalResolver principalResolver)
        {
            _principalResolver = principalResolver;
        }

        public int Resolve(object source, object destination, int sourceMember, int destMember, ResolutionContext context)
        {
            return GetProfileId(sourceMember) ?? 0;
        }

        public int? Resolve(object source, object destination, int? sourceMember, int? destMember, ResolutionContext context)
        {
            return GetProfileId(sourceMember.Value);
        }
        private int? GetProfileId(int? principalId)
        {
            if (!principalId.HasValue) return null;

            return _principalResolver?.GetPrincipalByIdentityId(principalId.Value)?.ProfileId;
        }
    }
}
