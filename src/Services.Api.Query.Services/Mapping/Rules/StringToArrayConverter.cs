﻿using System.Collections.Generic;
using AutoMapper;

namespace Services.Api.Query.Services.Mapping.Rules
{
    public class StringToArrayConverter : IMemberValueResolver<object, object, string, int[]>
    {
        public int[] Resolve(object source, object destination, string sourceMember, int[] destMember, ResolutionContext context)
        {
            var result = new List<int>();

            var companyIds = sourceMember?.Split(',') ?? new string[0];

            foreach (var companyId in companyIds)
            {
                if (int.TryParse(companyId, out int value))
                {
                    result.Add(value);
                }
            }

            return result.ToArray();
        }
    }
}
