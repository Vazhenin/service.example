﻿using Aeroclub.Services.Api.Query.Services.Interfaces;
using AutoMapper;
using System;
using System.Linq;

namespace Aeroclub.Services.Api.Query.Services.Mapping.Rules
{
    public class PostReadByProfileResolver : IValueResolver<Data.Projections.PostProjection, Model.Dto.Post, bool>
    {
        private readonly IPrincipalResolver _principalResolver;

        public PostReadByProfileResolver(IPrincipalResolver principalResolver)
        {
            this._principalResolver = principalResolver;
        }

        public bool Resolve(Data.Projections.PostProjection source, Model.Dto.Post destination, bool destMember, ResolutionContext context)
        {
            if (context.Items.TryGetValue(Data.Projections.PostProjection.ReadByTagName, out object readByObject) && int.TryParse(readByObject.ToString(), out int readByPrincipalId))
            {
                //Сообщение прочитано, если пользователь - собственник сообщения, либо собственник есть в числе прочитавшах
                return source.PostedByPrincipalId == readByPrincipalId || source.ReadByPrincipalIds.Contains(readByPrincipalId);
            }
            return false;
        }
    }
}
