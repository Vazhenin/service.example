﻿using System;
using System.Linq;
using Services.Api.Query.Model;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using AutoMapper;

namespace Services.Api.Query.Services
{
    public class PrincipalResolver : IPrincipalResolver
    {
        private readonly CabinetDbContext _cabinetDbContext;
        private readonly IPrincipalCache _principalCache;


        public PrincipalResolver(CabinetDbContext cabinetDbContext, IPrincipalCache principalCache, IMapper mapper)
        {
            _cabinetDbContext = cabinetDbContext;
            _principalCache = principalCache;
        }
        
        public Model.Dto.Principal GetPrincipalByIdentityId(int identityId)
        {
            Model.Dto.Principal result;
            result = _principalCache.GetPrincipalById(identityId);
            if (result != null)
            {
                return result;
            }
            Data.Entities.Principal principal;
            principal = _cabinetDbContext.Principals.FirstOrDefault(p => p.Id == identityId);

            if (principal != null)
            {
                result = new Model.Dto.Principal
                {
                    EmployeeId = principal.EmployeeId,
                    Id = principal.Id,
                    ProfileId = principal.ProfileId
                };

                _principalCache.AddPrincipal(result);
                return result;
            }
            return null;
        }

        public Model.Dto.Principal GetPrincipalByProfileId(int profileId)
        {
            Model.Dto.Principal result;
            result = _principalCache.GetPrincipalByProfileId(profileId);
            if (result != null)
            {
                return result;
            }
            var principal = _cabinetDbContext.Principals.FirstOrDefault(p => p.ProfileId == profileId);
            if (principal != null)
            {
                result = new Model.Dto.Principal
                {
                    EmployeeId = principal.EmployeeId,
                    Id = principal.Id,
                    ProfileId = principal.ProfileId
                };

                _principalCache.AddPrincipal(result);
                return result;
            }
            return null;
        }

        /// <summary>
        /// Получение идентификатора профиля по идентификатору принципала.
        /// </summary>
        /// <param name="principalId">Идентификтаор принципала.</param>
        /// <returns>Идентификатор профиля.</returns>
        public int? GetProfileIdByPrincipalId(int? principalId)
        {
            if (principalId.HasValue)
            {
                Model.Dto.Principal result;
                result = _principalCache.GetPrincipalById(principalId.Value);
                if (result != null)
                {
                    return result.ProfileId;
                }
                var principal = _cabinetDbContext.Principals.FirstOrDefault(x => x.Id == principalId.Value);
                if (principal != null)
                {
                    result = new Model.Dto.Principal
                    {
                        EmployeeId = principal.EmployeeId,
                        Id = principal.Id,
                        ProfileId = principal.ProfileId
                    };

                    _principalCache.AddPrincipal(result);
                    return result.ProfileId;
                }
            }
            return null;
        }
    }
}