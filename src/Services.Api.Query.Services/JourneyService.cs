﻿using Services.Api.Query.Model.Dto;
using System;
using Services.Api.Query.Services.Data;
using Services.Api.Query.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Services.Api.Query.Model.Dto.Search;
using Services.Api.Query.Model.Enum;
using Services.Api.Query.Services.Data.Entities;
using Services.Api.Query.Services.Data.Extensions.Queries;

namespace Services.Api.Query.Services
{
    public class JourneyService : IJourneyService
    {
        private readonly CabinetDbContext _cabinetDbContext;
        private readonly IMapper _mapper;
        private readonly IPrincipalResolver _principalResolver;

        public JourneyService(CabinetDbContext cabinetDbContext, IMapper mapper, IPrincipalResolver principalResolver)
        {
            _cabinetDbContext = cabinetDbContext;
            _mapper = mapper;
            _principalResolver = principalResolver;
        }


        public IEnumerable<Journey> SearchJourneysPaged(JourneyParameters dtoParameters)
        {
            var parameters = _mapper.Map<Model.Search.JourneyParameters>(dtoParameters);

            var query = _cabinetDbContext.Journeys.Include(i => i.BusinessTrips).AsNoTracking();

            if (parameters.JourneyNumber.HasValue)
            {
                query = query.Where(j => j.Number == parameters.JourneyNumber);
            }

            if (parameters.TripNumber.HasValue)
            {
                query = query.Where(j => j.BusinessTrips.Any(t => t.Number == parameters.TripNumber));
            }

            if (parameters.JourneyOrTripNumber.HasValue)
            {
                query = query.Where(j => j.Number == parameters.JourneyOrTripNumber || j.BusinessTrips.Any(t => t.Number == parameters.JourneyOrTripNumber));
            }

            if (parameters.CompanyIds?.Any() == true)
            {
                query = query.Where(j => j.BusinessTrips.Any(t => parameters.CompanyIds.Contains(t.CompanyId)));
            }

            if (parameters.ProfileIds?.Any() == true)
            {
                int[] principals = parameters.ProfileIds.Select(x => _principalResolver.GetPrincipalByProfileId(x).Id).ToArray();
                query = query.Where(j => j.BusinessTrips.Any(t => principals.Contains(t.CreatedByPrincipalId)));
            }

            if (parameters.DateFrom.HasValue)
            {
                var date = parameters.DateFrom.Value.Date;
                query = query.Where(j => j.StartsOn >= date);
            }

            if (parameters.DateTo.HasValue)
            {
                var date = parameters.DateTo.Value.Date.AddDays(1).AddMilliseconds(-1); // конец дня
                query = query.Where(j => j.EndsOn <= date);
            }

            if (parameters.StartsBeforeDate.HasValue)
            {
                var date = parameters.StartsBeforeDate.Value.Date;
                query = query.Where(j => j.StartsOn <= date);
            }

            if (parameters.EndsAfterDate.HasValue)
            {
                var date = parameters.EndsAfterDate.Value.Date;
                query = query.Where(j => j.EndsOn >= date);
            }

            if (parameters.TravelToCityId > 0)
            {
                query = query.Where(j => j.JourneyToCityId == parameters.TravelToCityId);
            }

            if (parameters.TravelFromCityId > 0)
            {
                query = query.Where(j => j.BusinessTrips.Any(t => t.TravelFromCityId == parameters.TravelFromCityId));
            }

            if (parameters.OrderNumber.HasValue && parameters.OrderNumber.Value > 0)
            {
                int? journeyId = GetJourneyIdByOrderNumber(parameters.OrderNumber.Value);
                if (journeyId.HasValue)
                {
                    query = query.Where(j => j.Id == journeyId);
                }
                else
                {
                    return new Journey[0].AsEnumerable(); // так как по номеру заказа поездка не найдена.
                }
            }

            if (parameters.StatusFilter?.Any() == true)
            {
                query = query.Where(j => j.BusinessTrips.Any(t => parameters.StatusFilter.Contains(t.Status)));
            }

            if (parameters.TravellerProfileId > 0)
            {
                query = query.Where(x => x.BusinessTrips.Any(y => y.ProfileId == parameters.TravellerProfileId));
            }

            if (parameters.TravellerProfileIds?.Any() == true)
            {
                query = query.Where(j => j.BusinessTrips.Any(t => parameters.TravellerProfileIds.Contains(t.ProfileId)));
            }

            if (parameters.ServiceState?.Any() == true)
            {
                var journeyIds = (from ss in _cabinetDbContext.Services.Where(ServiceStateHelper.GetServiceExpression(parameters.ServiceState))
                            join businessTripServices in _cabinetDbContext.BusinessTripServices on ss.Id equals businessTripServices.ServiceId
                            join businessTrip in _cabinetDbContext.BusinessTrips on businessTripServices.BusinessTripId equals businessTrip.Id
                            join qq in query on businessTrip.JourneyId equals qq.Id
                            select qq.Id).Distinct().ToList();
                if (journeyIds.Any())
                {
                    query = query.Where(x => journeyIds.Contains(x.Id));
                }
                else
                {
                    return new Journey[0].AsEnumerable(); // так как нет командировок с нужным типом.
                }
            }

            if (parameters.ServiceType?.Any() == true)
            {
                var journeyIds = (from qq in query
                                  join businessTrip in _cabinetDbContext.BusinessTrips on qq.Id equals businessTrip.JourneyId
                                  join businessTripServices in _cabinetDbContext.BusinessTripServices on businessTrip.Id equals businessTripServices.BusinessTripId
                                  join services in _cabinetDbContext.Services.Where(x => parameters.ServiceType.Contains(x.Type)) on businessTripServices.ServiceId equals services.Id
                                  select qq.Id).Distinct().ToList();
                if (journeyIds.Any())
                {
                    query = query.Where(x => journeyIds.Contains(x.Id));
                }
                else
                {
                    return new Journey[0].AsEnumerable(); // так как нет командировок с нужным типом.
                }
            }

            query = query.OrderByDescending(j => j.Number).Skip(parameters.PageNumber * parameters.PageSize).Take(parameters.PageSize);

            var journeys = _mapper.Map<ICollection<Journey>>(query);

            var servicesCounts = query.SelectMany(q =>
                q.BusinessTrips.Select(t =>
                    new TripServicesCount
                    {
                        BusinessTripId = t.Id,
                        ActiveServicesCount = t.BusinessTripServices.Where(bs => bs.BusinessTripId == t.Id).Count(s => s.Service.Status == ServiceStatus.Active),
                        ServicesCount = t.BusinessTripServices.Count(bs => bs.BusinessTripId == t.Id)
                    })).ToList();

            foreach (var journey in journeys)
            {
                foreach (var trip in journey.BusinessTrips)
                {
                    var servicesCount = servicesCounts.Find(c => c.BusinessTripId == trip.Id);
                    trip.ActiveServiceCount = (servicesCount?.ActiveServicesCount).GetValueOrDefault();
                    trip.ServiceCount = (servicesCount?.ServicesCount).GetValueOrDefault();
                    trip.JourneyNumber = journey.Number;
                }
            }
            return journeys;
        }

        /// <summary>
        /// Получение признака наличия непрочитанных сообщений.
        /// </summary>
        /// <param name="journeyNumber">Номер командировки.</param>
        /// <param name="businessTripNumber">Номер поездки.</param>
        /// <param name="profileId">Идентиифкатор профиля.</param>
        /// <returns>True если пост прочитан</returns>
        public bool GetPostUnReadStatusByNumbersAndProfile(int journeyNumber, int businessTripNumber, int profileId)
        {
            var principal = _principalResolver.GetPrincipalByProfileId(profileId);
            if (principal != null && principal.Id > 0)
            {
                return (from p in _cabinetDbContext.Posts
                        join bt in _cabinetDbContext.BusinessTrips on p.BusinessTripId equals bt.Id
                        join j in _cabinetDbContext.Journeys on bt.JourneyId equals j.Id
                        join pr in _cabinetDbContext.PostReceipts
                            .Where(pr => pr.ReadByPrincipalID == principal.Id) on p.Id equals pr.PostID into rds
                        from reads in rds.DefaultIfEmpty()
                        where (p.PostedByPrincipalId != principal.Id) && (j.Number == journeyNumber) && (bt.Number == businessTripNumber) && (reads == null)
                        select true).FirstOrDefault();
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Получить командировку по номеру заказа.
        /// </summary>
        /// <param name="orderNumber">Номер заказа.</param>
        /// <returns>Данные поездки.</returns>
        public Journey GetJourneyByOrderNumber(int orderNumber)
        {
            var query = _cabinetDbContext.Journeys.Include(i => i.BusinessTrips).AsNoTracking();
            int? journeyId = GetJourneyIdByOrderNumber(orderNumber);
            if (journeyId > 0)
            {
                query = query.Where(j => j.Id == journeyId);
                var result = _mapper.Map<Journey>(query.FirstOrDefault());
                if (result != null)
                {
                    foreach (var trip in result.BusinessTrips)
                    {
                        var services = _cabinetDbContext.GetTripServices(trip.Id);

                        trip.Services = services.ToArray();
                    }
                }
                return result;
            }
            else
            {
                return null; // так как по номеру заказа поездка не найдена.
            }
        }

        private int? GetJourneyIdByOrderNumber(int orderNumber)
        {
            int? journeyId = _cabinetDbContext.BusinessTripServices
                  .Where(x => x.Service.Order.Number == orderNumber)
                  .Select(x => x.BusinessTrip.JourneyId).FirstOrDefault();
            return journeyId;
        }
    }
}