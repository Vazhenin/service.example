﻿using System.Collections.Generic;
using Services.Api.Query.Model.Dto;
using Services.Api.Query.Model.Search;

namespace Services.Api.Query.Services.Interfaces
{
    public interface ITripPlanService
    {
        ICollection<ServiceCard> GetTripPlan(TripPlanParameters parameters);
    }
}