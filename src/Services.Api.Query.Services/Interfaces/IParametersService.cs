﻿using System.Collections.Generic;
using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IParametersService
    {
        int GetJourneyNumber(int journeyId);
        BusinessTripNumber GetTripNumbers(int tripId);
        object GetQueryParameters(string queryName, IEnumerable<KeyValuePair<string, string>> parameters);
    }
}