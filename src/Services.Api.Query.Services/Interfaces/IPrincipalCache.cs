﻿using Services.Api.Query.Model;
using Services.Api.Query.Model.Dto;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IPrincipalCache
    {
        Principal GetPrincipalByEmployeeId(int employeeId);
        Principal GetPrincipalByProfileId(int profileId);
        Principal GetPrincipalById(int principalId);
        void AddPrincipal(Principal principal);
    }
}