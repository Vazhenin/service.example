﻿using Services.Api.Query.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IQueryCommand
    {

    }

    public interface IQueryCommand<TQueryParameters, TResult> : IQueryCommand where TQueryParameters : IQueryParameters<TResult>
    {
        TResult Execute(TQueryParameters query);
    }
}
