﻿using Services.Api.Query.Model.Dto;
using System.Collections.Generic;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IJourneyService
    {
        IEnumerable<Journey> SearchJourneysPaged(JourneyParameters parameters);

        /// <summary>
        /// Получить командировку по номеру заказа.
        /// </summary>
        /// <param name="orderNumber">Номер заказа.</param>
        /// <returns>Данные поездки.</returns>
        Journey GetJourneyByOrderNumber(int orderNumber);

        /// <summary>
        /// Получение признака наличия непрочитанных сообщений.
        /// </summary>
        /// <param name="journeyNumber">Номер командировки.</param>
        /// <param name="businessTripNumber">Номер поездки.</param>
        /// <param name="profileId">Идентиифкатор профиля.</param>
        /// <returns>True если пост прочитан</returns>
        bool GetPostUnReadStatusByNumbersAndProfile(int journeyNumber, int businessTripNumber, int profileId);
    }
}