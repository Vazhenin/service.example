﻿using Services.Api.Query.Model;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IPrincipalResolver
    {
        Model.Dto.Principal GetPrincipalByProfileId(int profileId);

        Model.Dto.Principal GetPrincipalByIdentityId(int identityId);

        /// <summary>
        /// Получение идентификатора профиля по идентификатору принципала.
        /// </summary>
        /// <param name="principalId">Идентификтаор принципала.</param>
        /// <returns>Идентификатор профиля.</returns>
        int? GetProfileIdByPrincipalId(int? principalId);
    }
}