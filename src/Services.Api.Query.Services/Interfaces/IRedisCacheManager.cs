﻿using System;

namespace Services.Api.Query.Services.Interfaces
{
    public interface IRedisCacheManager
    {
        bool TryGetValue<T>(string key, out T value, int dbIndex = -1);
        void AddOrUpdateValue<T>(string key, T value, TimeSpan lifetime, int dbIndex = -1);
        bool Contains(string key, int dbIndex = -1);
        void Remove(string key, int dbIndex = -1);
    }
}