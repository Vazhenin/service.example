﻿using Services.Api.Query.Model.Dto;
using System.Collections.Generic;
using Services.Api.Query.Model.Dto.Search;

namespace Services.Api.Query.Services.Interfaces
{
    public interface ITripService
    {
        ICollection<Post> GetTripPosts(int journeyNumber, int businessTripNumber, PostParameters postParameters);

        PostInfo GetTripPost(int tripId, int postId);
        ICollection<Document> GetTravellerDocumentsForTrip(int journeyNumber, int tripNumber);

        BusinessTrip GetTripByNumber(int journeyNumber, int tripNumber);

        /// <summary>
        /// Получение признака наличия непрочитанных сообщений.
        /// </summary>
        /// <param name="tripId">Идентиифкатор командировки.</param>
        /// <param name="profileId">Идентификатор профиля.</param>
        /// <returns>True если пост прочитан</returns>
        bool GetPostUnReadStatusByBusinessTripeIdsAndProfile(int tripId, int profileId);

        BusinessTripInfo GetTripForAuthorization(int tripId);

        BusinessTrip GetTripForAuthorizationDetail(int tripId);
    }
}